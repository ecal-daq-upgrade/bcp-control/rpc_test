#include "DeviceDriver.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

namespace
{
using namespace drivers;

class MyPoke : public DeviceDriver {
public:
	MyPoke(const std::string &device_name) : DeviceDriver(device_name) {
    this->addKnownConfigField("value", true, "0");
    this->addKnownConfigField("devnode", true, "");
    this->addKnownConfigField("nodetype", true, "");
    this->addKnownConfigField("offset", true, "0");
	};

	int offset;
	int value;
  std::string nodetype;
  std::string devnode;

	virtual void parseConfiguration(const ini::IniSection &section, const apx_prime::GetBoardInfoResponse::DeviceConfiguration &board_config) {
		DeviceDriver::parseConfiguration(section, board_config);
		if (section.count("value"))
			this->value = section.at("value").as<int>();
		else
			throw invalid_driver_config_error("No GPIO poke value found");
    if (section.count("devnode"))
      this->devnode = section.at("devnode").as<std::string>();
    else
      throw invalid_driver_config_error("No GPIO poke devnode found");
    if (section.count("nodetype"))
      this->nodetype = section.at("nodetype").as<std::string>();
    else
      throw invalid_driver_config_error("No nodetype found");
    if (section.count("offset"))
      this->offset = section.at("offset").as<int>();
	};

	virtual void configureDevice(wisc::RPCSvc &rpcsvc) {

    if (!rpcsvc.load_module("mypoke", "mypoke v0.0.0")) {
      fprintf(stderr, "Unable to load mypoke RPCSvc module");
      return;
    }
    if (!rpcsvc.load_module("mymemory", "mymemory v1.0.2")) {
      fprintf(stderr, "Unable to load mypoke RPCSvc module");
      return;
    }

    std::cout<< "Here we preare and launch the request to the GPIO Poke module" <<std::endl;
    // Do we really need to create a message like this?
   
		wisc::RPCMsg req;
    if (this->nodetype=="gpio") {
		  req = wisc::RPCMsg("mypoke.configure");
    } else if (this->nodetype=="fpga") {
		  req = wisc::RPCMsg("mymemory.mwrite");
		  req.set_word("offset", this->offset);
    }
		req.set_word("value", this->value);
    req.set_string("devnode", this->devnode);
		rpcsvc.call_method(req);
    
    // This is a fake call to let the system continue
    apx_prime::ToggleResetGPIORequest req1;
    req1.set_device(this->device_name);
    rpcmsg2proto<void>(rpcsvc.call_method(proto2rpcmsg("mypoke.configure", req1))); 
	};

/*	virtual DiagnosticStatus retrieveStatus(wisc::RPCSvc &rpcsvc) {
    std::cout<< "Here we check the status of the GPIO Poke device" <<std::endl;
    DiagnosticStatus::StatusCode code = DiagnosticStatus::STATUS_UNKNOWN;
    std::string status_short = "The gpio status information could not be parsed.";
    std::string status_lines = "the status";
		/*apx_prime::GetSi534xStatusRequest req;
		req.set_device(this->device_name);
		apx_prime::GetSi534xStatusResponse rsp = rpcmsg2proto<apx_prime::GetSi534xStatusResponse>(rpcsvc.call_method(proto2rpcmsg("apx-prime.get_si534x_status", req)));
		std::istringstream status_lines(rsp.status());
		DiagnosticStatus::StatusCode code = DiagnosticStatus::STATUS_UNKNOWN;
		std::string status_short = "The device status information could not be parsed.";
		int dspll_is_line = 0;
		while (!status_lines.eof() && !status_lines.fail()) {
			std::string line;
			std::getline(status_lines, line);
			if (line.substr(0, 9) != "DSPPL is ")
				continue;
			if (++dspll_is_line != 2)
				continue;
			if (line.substr(9, 5) == "\x1B[32m")
				code = DiagnosticStatus::STATUS_GOOD;
			else if (line.substr(9, 5) == "\x1B[31m")
				code = DiagnosticStatus::STATUS_BAD;
			status_short = line;
			ini::trim(status_short);
		}
		return DiagnosticStatus(code, status_short, rsp.status());
    return DiagnosticStatus(code, status_short, status_lines);
	};*/
};

REGISTER_DEVICE_DRIVER("mypoke", MyPoke)

} // namespace
