#include "DeviceDriver.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "ResetableI2CDevice.h"

namespace
{
using namespace drivers;

class Myi2c : public DeviceDriver {
public:
	Myi2c(const std::string &device_name) : DeviceDriver(device_name), i2cmgr(device_name) {
    this->i2cmgr.extendKnownConfigFields(*this);
	};

  I2CVectorManager i2cmgr;


	virtual void parseConfiguration(const ini::IniSection &section, const apx_prime::GetBoardInfoResponse::DeviceConfiguration &board_config) {
		DeviceDriver::parseConfiguration(section, board_config);
    this->i2cmgr.parseConfiguration(section, board_config);
	};

	virtual void configureDevice(wisc::RPCSvc &rpcsvc) {
    this->i2cmgr.configureDevice(rpcsvc);
	};

};

REGISTER_DEVICE_DRIVER("myi2c", Myi2c)

} // namespace
