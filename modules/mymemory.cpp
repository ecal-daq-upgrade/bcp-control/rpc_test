#include <errno.h>
#include <libeasymem.h>
#include "rpcsvc/moduleapi.h"
#include <iomanip>
#include <iostream>
#include <chrono>

static void *memrange = NULL;

/*void mread(const RPCMsg *request, RPCMsg *response) {
	uint32_t count = request->get_word("count");
	uint32_t addr = request->get_word("address");
	uint32_t data[count];

	if (easymem_saferead32(memrange, addr, count, data, 1) == 0) {
		response->set_word_array("data", data, count);
	}
	else {
		response->set_string("error", stdsprintf("Easymem error: errno %d", errno));
		LOGGER->log_message(LogManager::INFO, stdsprintf("read easymem error: %d", errno));
	}
}*/

void mwrite(const RPCMsg *request, RPCMsg *response) {

        if (easymem_map_uio(&memrange, request->get_string("devnode").c_str(), 0, 0xFFFFFF, 0) != 0) {
                LOGGER->log_message(LogManager::ERROR, stdsprintf("Unable to map UIO: errno %d", errno));
                LOGGER->log_message(LogManager::ERROR, "Unable to load module");
                return; // Do not register our functions, we depend on that mapping.
        }

	uint32_t data[1];
	uint32_t addr = request->get_word("offset");
        data[0] = request->get_word("value");
	if (easymem_safewrite32(memrange, addr, 1, data, 1) != 0) {
		response->set_string("error", stdsprintf("Easymem error: errno %d", errno));
		LOGGER->log_message(LogManager::INFO, stdsprintf("write easymem error: errno %d", errno));
	}
}

extern "C" {
const char *module_version_key = "mymemory v1.0.2";
int module_activity_color = 0xff0066;
int module_led_id = 0;
void module_init(ModuleManager *modmgr) {
        LOGGER->log_message(LogManager::INFO, stdsprintf("Init module mymemory"));

	//modmgr->register_method("mymemory", "read", mread);
	modmgr->register_method("mymemory", "mwrite", mwrite);
}
}

