#include <errno.h>
#include <libeasymem.h>
#include <rpcsvc/moduleapi.h>
#include <gpiod.hpp>

static void *memrange = NULL;

void mread(const RPCMsg *request, RPCMsg *response) {
	uint32_t count = request->get_word("count");
	uint32_t addr = request->get_word("address");
	uint32_t data[count];

	if (easymem_saferead32(memrange, addr, count, data, 1) == 0) {
		response->set_word_array("data", data, count);
	}
	else {
		response->set_string("error", stdsprintf("Easymem error: errno %d", errno));
		LOGGER->log_message(LogManager::INFO, stdsprintf("read easymem error: %d", errno));
	}
}

void configure(const RPCMsg *request, RPCMsg *response) {
        LOGGER->log_message(LogManager::INFO, stdsprintf("mypoke configure function"));
	if (request->get_key_exists("proto")) {
		LOGGER->log_message(LogManager::INFO, stdsprintf("proto argument found, I know it is a fake call"));
                response->set_string("status", "this is a fake call");
                return;
	}
	if (!request->get_key_exists("value")) {
		LOGGER->log_message(LogManager::ERROR, stdsprintf("value argument not found in request"));
                response->set_string("rpcerror", "value key not found");
                return;
        }
	if (!request->get_key_exists("devnode")) {
                LOGGER->log_message(LogManager::ERROR, stdsprintf("devnode argument not found in request"));
                response->set_string("rpcerror", "devnode key not found");
                return;
        }
	std::string devnode = request->get_string("devnode");
	int value = request->get_word("value");

	LOGGER->log_message(LogManager::INFO, "Devnode " + devnode);
	LOGGER->log_message(LogManager::INFO, stdsprintf("Value %d", value));

	::gpiod::chip chip(devnode);
	int num_lines = chip.num_lines();
	LOGGER->log_message(LogManager::INFO, "Name " + chip.name());
	LOGGER->log_message(LogManager::INFO, "Label " + chip.label());
	LOGGER->log_message(LogManager::INFO, stdsprintf("Num lines %d", num_lines));


	::gpiod::line_bulk all_lines = chip.get_all_lines();
	::gpiod::line_request myrequest;
        myrequest.consumer = "mypoke module";
        myrequest.request_type = ::gpiod::line_request::DIRECTION_AS_IS;
	all_lines.request(myrequest);

	std::vector<int> values;
	for (int i=0; i<num_lines; ++i) {
	  int single_val = (value >> i) & 0x1;
	  LOGGER->log_message(LogManager::INFO, stdsprintf("Single val %d", single_val));
          values.push_back(single_val); 
	}

        all_lines.set_values(values);

	all_lines.release();
}

extern "C" {
const char *module_version_key = "mypoke v0.0.0";
int module_activity_color = 0xff0067;
int module_led_id = 0;
void module_init(ModuleManager *modmgr) {
	modmgr->register_method("mypoke", "configure", configure);
}
}
