// Compile and deploy the module
//
// g++   -fPIC -shared -o fwmodule.so fwmodule.cpp -I../rpc_bcp_fw/
// sudo cp fwmodule.so /usr/lib/rpcsvc/

#include <errno.h>
#include <libeasymem.h>
#include "rpcsvc/moduleapi.h"
#include "BCP.h"
#include <iomanip>
#include <iostream>
#include <chrono>

BCP *bcp = NULL;

std::vector<int> split(const std::string &s, char delimiter) {     
    std::vector<int> tokens;     
    std::string token;     
    std::istringstream tokenStream(s);     
    while (getline(tokenStream, token, delimiter)) {      
        tokens.push_back(stoi(token));     
    }     
    return tokens;  
}


void configurefe(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start configurefe");
    auto start = std::chrono::high_resolution_clock::now();
    std::string lpgbts_json_string = "";
    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers");
    if (request->get_key_exists("lpgbtsjsonstring")) lpgbts_json_string = request->get_string("lpgbtsjsonstring");

    std::vector<int> res = split(towers, ',');   
    
    for (auto tower: res) {
	  std::string result = bcp->towers.at(tower)->configureFE(lpgbts_json_string);
      std::string my_key = "tc_links_" + std::to_string(tower);
      response->set_string(my_key, result);
    }
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}

void fastresetvfe(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start fastresetvfe");
    auto start = std::chrono::high_resolution_clock::now();
    
    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers");
    std::vector<int> res = split(towers, ',');
    for (auto tower: res)
    	bcp->towers.at(tower)->fastResetVFE();

    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}

void configurefesfromsinglefile(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start loading configuration of fes");
    std::string config_json_string = "";
    if (request->get_key_exists("configjsonstring")) config_json_string = request->get_string("configjsonstring");
    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers");
    // we expect a json file with this structure:
    // { "towers": 
    //     "0": { "lpgbts":
    //       "113": { "0": 0,
    //                "1": 100  // where these pairs are reg_add: reg_val in decimal
    //                ... },
    //       "114": { ...
    //
    // Remeber that all keys are string!
   
    std::vector<int> res = split(towers, ',');  
    auto start = std::chrono::high_resolution_clock::now();

    json data;
    if (config_json_string == "") {
      LOGGER->log_message(LogManager::ERROR, "configurefesfromsinglefile: Empty json string");
    } else {
      data = json::parse(config_json_string);
    }

    for (auto tower: res) {
        if (data.contains("towers") && data["towers"].contains(std::to_string(tower))) {
            
          std::string result = bcp->towers.at(tower)->configureFE(data["towers"][std::to_string(tower)]["lpgbts"].dump());
          std::string my_key = "tc_links_" + std::to_string(tower);
          response->set_string(my_key, result);

        } else {
            LOGGER->log_message(LogManager::ERROR, stdsprintf("configurefesfromsinglefile: No key \"towers\" in the json file or no tower %d in \"towers\"", tower));
        }
    }
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));

}

void configurevfesfromsinglefile(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start loading configuration of vfes");
    std::string config_json_string = "";
    if (request->get_key_exists("configjsonstring")) config_json_string = request->get_string("configjsonstring");
    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers"); 

    std::vector<int> res = split(towers, ',');

    //LOGGER->log_message(LogManager::INFO, config_json_string);
    auto start = std::chrono::high_resolution_clock::now();
    // With the old CATIA we always read 255. It is up to the user to provide the correct value
    // in the config_json_string

    json data;
    if (config_json_string == "") {
      LOGGER->log_message(LogManager::ERROR, "configurevfesfromsinglefile: Empty json string");
    } else {
      data = json::parse(config_json_string);
    }

    for (auto tower: res) {
        if (data.contains("towers") && data["towers"].contains(std::to_string(tower))) {
            bcp->towers.at(tower)->configureVFEsFromSingleFile(data["towers"][std::to_string(tower)]);
        } else {
            LOGGER->log_message(LogManager::ERROR, stdsprintf("configureVFEsFromSingleFile: No key \"towers\" in the json file or no tower %d in \"towers\"", tower));
        }
    }
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}

void losetime(const RPCMsg *request, RPCMsg *response) {
    std::string mystring = request->get_string("mystring");
    LOGGER->log_message(LogManager::INFO, "Losetime start " + mystring);
    int fwversion = readFPGA(bcp->fwpointer, "/firmware");
    sleep(10);
    LOGGER->log_message(LogManager::INFO, "Losetime end " + mystring + " " + std::to_string(fwversion));

}

void prepareDTUalign(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start prepareDTUalign");

    // We need it to reset some registers used during our tests
    bcp->setDefaultRegisters();

    std::string config_json_string = "";
    if (request->get_key_exists("configjsonstring")) config_json_string = request->get_string("configjsonstring");
    //LOGGER->log_message(LogManager::INFO, config_json_string);
    json data;
    if (config_json_string == "") {
      LOGGER->log_message(LogManager::ERROR, "prepareDTUalign: Empty json string");
    } else {
      data = json::parse(config_json_string);
    }
    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers");
   
    std::vector<int> res = split(towers, ',');
    for (auto tower: res) {
        if (data.contains("towers") && data["towers"].contains(std::to_string(tower))) {
            bcp->towers.at(tower)->prepareDTUalign(data["towers"][std::to_string(tower)]);
        } else {
            LOGGER->log_message(LogManager::ERROR, stdsprintf("prepareDTUalign: No key \"towers\" in the json file or no tower %d in \"towers\"", tower));
        }
    }
    LOGGER->log_message(LogManager::INFO, "End prepareDTUalign");
}

void normalmode(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start normalmode");
    auto start = std::chrono::high_resolution_clock::now();
    uint32_t tower = 0;
    if (request->get_key_exists("towernb")) tower = request->get_word("towernb");
    bcp->towers.at(tower)->normalMode();
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}


// the response is a json in the form
// {
//  "towers": {
//   "0": {
//    "channels": {             // tower number
//     "0": {           // channel number
//      "dtu": [],      // dtu registers from 0 to 14 included
//      "catia": [],    // catia registers from 0 to 6 included
//      "adc0": [],     // adc0 registers from 0 to 75 included
//      "adc1": [] },   // adc1 registers from 0 to 75 included 
//     }, "1": {           // next channel number, same structure
//     ...
//     }, "25" {}          // last channel of the tower
//    }
//   },"1": {}            // next tower, same structure
//  }
// }
void getAllVFERegisters(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start getAllVFERegisters");
    json registers;
    registers["towers"] = {};

    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers");
    std::vector<int> res = split(towers, ',');
    for (auto tower: res)
      registers["towers"][std::to_string(tower)] = bcp->towers.at(tower)->getAllVFERegisters();

    response->set_string("registers", registers.dump());
    LOGGER->log_message(LogManager::INFO, registers.dump());
    LOGGER->log_message(LogManager::INFO, "End getAllVFERegisters");
}

void pedestalscan(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start pedestalscan");
    auto start = std::chrono::high_resolution_clock::now();
    bcp->towers.at(0)->pedestalscan();
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}

void errorstatus(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start errorstatus");
    auto start = std::chrono::high_resolution_clock::now();
    json errors;
    errors["towers"] = {};

    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers");
    std::vector<int> res = split(towers, ',');
    for (auto tower: res)
      errors["towers"][tower] = bcp->towers.at(tower)->getErrorStatus();
    response->set_string("errors", errors.dump());
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, errors.dump());
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}

void getsingleerror(const RPCMsg *request, RPCMsg *response) {

    LOGGER->log_message(LogManager::INFO, "Start errorstatus");
    auto start = std::chrono::high_resolution_clock::now();
    uint32_t tower = 0;
    std::string errorname = "bc0_c";
    json errors;
    if (request->get_key_exists("towernb")) tower = request->get_word("towernb");
    if (request->get_key_exists("errorname")) errorname = request->get_string("errorname");
    errors = bcp->towers.at(tower)->getSingleError(errorname);
    response->set_string("errors", errors.dump());
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, errors.dump());
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
} 

void askerrorsnames(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start errorstatus");
    auto start = std::chrono::high_resolution_clock::now();
    uint32_t tower = 0;
    json errors = bcp->towers.at(tower)->getErrorList();
    response->set_string("errors", errors.dump());
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, errors.dump());
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}

void getbc0scan(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Start bc0 scan");
    auto start = std::chrono::high_resolution_clock::now();

    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers");

    std::vector<int> res = split(towers, ',');
    
    /*json bc0scan;
    bc0scan["towers"] = {};

    for (auto tower: res) {
      bcp->towers.at(tower)->getBc0ScanList();
      bc0scan["towers"][tower] = {};
      bc0scan["towers"][tower] = bcp->towers.at(tower)->jsonifyBc0Scan();
    }*/
    json bc0scan = bcp->getbc0scan(res);
    response->set_string("bc0scan", bc0scan.dump());
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, bc0scan.dump());
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}

void resetbc0align(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Reset bc0 align");
    auto start = std::chrono::high_resolution_clock::now();
    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers");
    std::vector<int> res = split(towers, ',');
    for (auto tower: res)
    	bcp->towers.at(tower)->resetBc0Align();
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}

void resyncAllTowers(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Started resync all towers");
    auto start = std::chrono::high_resolution_clock::now();
    int manual_value = manual_value = request->get_word("manualvalue");
    bcp->resyncAllTowers(manual_value);
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}

/*void bc0align(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Started bc0align");
    auto start = std::chrono::high_resolution_clock::now();
    std::string towers = "0";
    if (request->get_key_exists("towers")) towers = request->get_string("towers");
    std::vector<int> res = split(towers, ',');
    
    int manual_value = -999;
    if (request->get_key_exists("manualvalue")) manual_value = request->get_word("manualvalue");
    if (manual_value == -999) {
        //LOGGER->log_message(LogManager::INFO, stdsprintf("Manual value %d, starting auto align",manual_value));
        LOGGER->log_message(LogManager::INFO, "Autoalign disabled for the moment");
        //bcp->autoalignBc0(res);
    } else {
        LOGGER->log_message(LogManager::INFO, stdsprintf("Manual value %d, starting manual align",manual_value));
        bcp->alignBc0(res, manual_value);
    }
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));
}*/

void newbc0align(const RPCMsg *request, RPCMsg *response) {
    LOGGER->log_message(LogManager::INFO, "Started newbc0align");
    auto start = std::chrono::high_resolution_clock::now();
    std::string towers = "0";
    int manual_value = -999;
    if (request->get_key_exists("manualvalue")) manual_value = request->get_word("manualvalue");
    if (manual_value ==-999) {
        LOGGER->log_message(LogManager::ERROR, stdsprintf("ERROR with BC0 align value"));
    } else {
        LOGGER->log_message(LogManager::INFO, stdsprintf("Manual value %d, starting manual align",manual_value));
        bcp->newalignBc0( manual_value);
    }
    auto stop = std::chrono::high_resolution_clock::now();

}

void crash(const RPCMsg *request, RPCMsg *response) {

    std::cout<<"You should see this message"<<std::endl;
    *(int*)0=0;
    std::cout<<"You should not see this message"<<std::endl;
}

void ping(const RPCMsg *request, RPCMsg *response) {

    std::cout<<"Ping... Pong..."<<std::endl;
    response->set_string("pong", "pong");
}

extern "C" {
const char *module_version_key = "fwmodule v1.0.2";
int module_activity_color = 0xff0066;
int module_led_id = 0;
void module_init(ModuleManager *modmgr) {
    auto start = std::chrono::high_resolution_clock::now();
    bcp = new BCP(LOGGER);

    modmgr->register_method("fwmodule", "configurefe", configurefe);
    modmgr->register_method("fwmodule", "fastresetvfe", fastresetvfe);
    modmgr->register_method("fwmodule", "configurefesfromsinglefile", configurefesfromsinglefile);
    modmgr->register_method("fwmodule", "configurevfesfromsinglefile", configurevfesfromsinglefile);
    modmgr->register_method("fwmodule", "losetime", losetime);
    modmgr->register_method("fwmodule", "prepareDTUalign", prepareDTUalign);
    modmgr->register_method("fwmodule", "normalmode", normalmode);
    modmgr->register_method("fwmodule", "getAllVFERegisters", getAllVFERegisters);
    modmgr->register_method("fwmodule", "pedestalscan", pedestalscan);
    modmgr->register_method("fwmodule", "errorstatus", errorstatus);
    modmgr->register_method("fwmodule", "getbc0scan", getbc0scan);
    modmgr->register_method("fwmodule", "getsingleerror", getsingleerror);
    modmgr->register_method("fwmodule", "newbc0align", newbc0align);
    modmgr->register_method("fwmodule", "askerrorsnames", askerrorsnames);
    modmgr->register_method("fwmodule", "resyncAllTowers", resyncAllTowers);
    modmgr->register_method("fwmodule", "crash", crash);
    modmgr->register_method("fwmodule", "ping", ping);

//    modmgr->register_method("fwmodule", "bc0align", bc0align);
    auto stop = std::chrono::high_resolution_clock::now();
    LOGGER->log_message(LogManager::INFO, stdsprintf("Time required by module_init %d", std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count()));

}
}
