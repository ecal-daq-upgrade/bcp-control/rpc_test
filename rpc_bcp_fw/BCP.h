#ifndef BCP_H
#define BCP_H

#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <typeinfo>

#include "json.hpp"
#include "fw.h"
#include "basic_functions.h"
#include "lpgbt_files/enum_lpgbt_reg_map.h"
#include "Tower.h"


class BCP {
public:
    BCP(LogManager* outlogger) {
        LOGGER = outlogger;
        LOGGER->log_message(LogManager::INFO, "In BCP constructor");

        if (!FALCON) {
          fw_version = getFirmware("/dev/uio.axi_chip2chip_0");
          LOGGER->log_message(LogManager::INFO, stdsprintf("Fw version 0x%08x", fw_version));
        }

        std::string filepath = "";
        if (FALCON) {
           filepath = "/tmp/register_map_falcon8.csv";
        } else {
          std::stringstream ss;
          ss << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << fw_version;
          filepath = "/var/firmware_files/register_map_"+ss.str()+".csv";
        }
        fwpointer = new Fw(LOGGER, filepath);
        
        
        if (FALCON) number_of_towers = 1;
        else number_of_towers = fw_version & 0xFF;
        for (int nt=0; nt<number_of_towers; nt++) {
            towers.push_back(new Tower(LOGGER, fwpointer, nt));
        }
        number_of_groups = number_of_towers / towers_per_group;
    };

    uint32_t fw_version;
    int number_of_towers;
    int towers_per_group = 3;
    std::vector<Tower*> towers;
    Fw * fwpointer;
    LogManager * LOGGER;
    int number_of_groups;
    int bc0AlignPoint = 0;

    void resyncAllTowers(int action) {

        //FIXME are we operating over tower 0 only?????


        if (action == 1) resyncBCP();
        if (action == 2) flushDTUs();
        if (action == 3) resetBCPCounters();
        if (action == 4) {
            resyncBCP();
            
            //json bc0s_all_towers;
            //towers.at(0)->getBc0ScanList();
            //bc0s_all_towers[0] = towers.at(0)->jsonifyBc0Scan();
            //LOGGER->log_message(LogManager::INFO, "bc0 scan before flushDTUs");
            //LOGGER->log_message(LogManager::INFO, bc0s_all_towers.dump());
           
            flushDTUs();
            
            //towers.at(0)->getBc0ScanList();
            //bc0s_all_towers[0] = towers.at(0)->jsonifyBc0Scan();
            //LOGGER->log_message(LogManager::INFO, "bc0 scan after resetBCPCounters");
            //LOGGER->log_message(LogManager::INFO, bc0s_all_towers.dump());

            resetBCPCounters();
       }
   }

   void setDefaultRegisters() {
     for(int tg=0; tg < number_of_groups; tg++) {
          std::ostringstream tg_string;
          tg_string << stdsprintf("/BCP_Lpgbt/tg%d/", tg);

          writeFPGA(fwpointer, tg_string.str()+"threshold", 0xFFFF);
     }

   }

    json getbc0scan(std::vector<int> tower_list) {
        
        for (uint32_t cnt=bc0AlignPoint; cnt<=800; cnt++){
            writeFPGA(fwpointer, "/BCP_TCDS/bc0_scan_register", cnt);
            for (auto tower: tower_list) {
                towers.at(tower)->getBc0(cnt);
            }
        }
        json bc0scan;
        bc0scan["towers"] = {};
        for (auto tower: tower_list) {
            bc0scan["towers"][std::to_string(tower)] = {};
            bc0scan["towers"][std::to_string(tower)] = towers.at(tower)->jsonifyBc0Scan();
        }
        return bc0scan;
    }

   void newalignBc0(int value) {
     for(int tg=0; tg < number_of_groups; tg++) {
       std::ostringstream tg_string;
       tg_string << stdsprintf("/BCP_Lpgbt/tg%d/", tg);
	   writeFPGA(fwpointer, tg_string.str()+"chosen_align", value);
       writeFPGA(fwpointer, tg_string.str()+"rst_align", 1);
       writeFPGA(fwpointer, tg_string.str()+"clear_align", 1);
	   writeFPGA(fwpointer, tg_string.str()+"rst_align", 0);
       writeFPGA(fwpointer, tg_string.str()+"clear_align", 0);
	 }
     bc0AlignPoint = value;
     for(auto tower:towers) {
       tower->setBc0AlignPoint(value);
     }
   }

   void resyncBCP(){
       LOGGER->log_message(LogManager::INFO, "Starting resyncBCP");
       for(int tg=0; tg < number_of_groups; tg++) {
          std::ostringstream tg_string;
          tg_string << stdsprintf("/BCP_Lpgbt/tg%d/", tg);

          writeFPGA(fwpointer, tg_string.str()+"rst_cdc", 0x01);
          //usleep(5000);
          writeFPGA(fwpointer, tg_string.str()+"rst_cdc", 0x00);
          //usleep(5000);
          for (int tower_in_gr=0; tower_in_gr<towers_per_group; tower_in_gr++) {
              std::ostringstream tower_string;
              tower_string << tg_string.str() << stdsprintf("t%d/", tower_in_gr);
              writeFPGA(fwpointer, tower_string.str()+"maskCh", 0x00000000);
              //usleep(5000);
              for (int ch=0; ch<25; ch++){
                  std::ostringstream decomp_mod_string;
                  decomp_mod_string << tower_string.str() << stdsprintf("ch%d/decomp_module/", ch);
                  //LOGGER->log_message(LogManager::INFO, decomp_mod_string.str());
                  writeFPGA(fwpointer, decomp_mod_string.str()+"wr_pointer", 0x78);
                  //usleep(5000); Do we need it ?
              }
          }
          writeFPGA(fwpointer, tg_string.str()+"rst_fsm", 0x01);
          //usleep(5000);
          writeFPGA(fwpointer, tg_string.str()+"rst_fsm", 0x00);
          //usleep(5000);
       }
   }

   void flushDTUs() {
       LOGGER->log_message(LogManager::INFO, "Starting flushDTUs");
       for(auto tower:towers) {
           //usleep(5000);
           tower->setFastCmd(0x7);
       }
       fcmdStrobe(fwpointer);
       //fcmdStrobe(fwpointer); // MANY STROBES GENERATE PROBLEMS
       LOGGER->log_message(LogManager::INFO, "After fcmdStrobe");
       for(auto tower:towers) {
         tower->fcmdCtrlReset();
       }
       //sleep(2);
   }

   void resetBCPCounters() {
       for(int tg=0; tg < number_of_groups; tg++) {
           std::ostringstream tg_string;
           tg_string << stdsprintf("/BCP_Lpgbt/tg%d/", tg);

           writeFPGA(fwpointer, tg_string.str()+"clear_cnt_mon", 0x01);
           usleep(5000);
           writeFPGA(fwpointer, tg_string.str()+"clear_cnt_mon", 0x00);
           usleep(5000);
           writeFPGA(fwpointer, tg_string.str()+"clear_cnt_decom", 0x01);
           usleep(5000);
           writeFPGA(fwpointer, tg_string.str()+"clear_cnt_decom", 0x00);
           usleep(5000);
       }
   }

    int getBc0Diff(json &bc0s, json &diffs, std::vector<int> &excludedValues) {
        // find max excluding values in excludedValues
        int myMax = 0;
        for (auto &tower: bc0s.items()) {
          for (auto &ch: tower.value().items()) {
            if ((std::find(excludedValues.begin(), excludedValues.end(), ch.value()) == excludedValues.end()) && (ch.value()>myMax)) myMax = ch.value();
          }
        }
        // calculate diffs
        for (auto &tower: bc0s.items()) {
          for (auto &ch: tower.value().items()) {
              int myval = ch.value();
              diffs[tower.key()][ch.key()] = myMax-myval;
          }
        }
        return myMax;
    }


    // Old align BC0, not working any more
    /*void alignBc0(std::vector<int> selectedTowers, int manual_value) {
        json bc0s_all_towers;
        json diffs;

        //for (auto tower: selectedTowers) {
        //    towers.at(tower)->getBc0ScanList();
        //    bc0s_all_towers[tower] = towers.at(tower)->jsonifyBc0Scan();
        //}
        //LOGGER->log_message(LogManager::INFO, "bc0 scan before reset");
        //LOGGER->log_message(LogManager::INFO, bc0s_all_towers.dump());
        for (auto tower: selectedTowers) {
            towers.at(tower)->resetBc0Align();
        }
        //sleep(2);

        for (auto tower: selectedTowers) {
            towers.at(tower)->getBc0ScanList();
            bc0s_all_towers[tower] = towers.at(tower)->jsonifyBc0Scan();
        }
        LOGGER->log_message(LogManager::INFO, "bc0 scan after reset, before set delay");
        LOGGER->log_message(LogManager::INFO, bc0s_all_towers.dump());

        for (auto tower: selectedTowers) {
            //LOGGER->log_message(LogManager::INFO, tower.key());
            diffs[tower] = {};
            for (int ch = 0; ch<25; ch++) {
              diffs[tower][std::to_string(ch)] = -1;
            }
        }
        LOGGER->log_message(LogManager::INFO, "bc0align after init diffs");
        for (auto &tower: bc0s_all_towers.items()) {
          for (auto &ch: tower.value().items()) {
              int myval = ch.value();

              diffs[std::stoi(tower.key())][ch.key()] = manual_value-myval;
          }
        }
        LOGGER->log_message(LogManager::INFO, "bc0align after calc diffs");
        for (auto tower: selectedTowers) {
            towers.at(tower)->setBc0Align(diffs[tower]);
        }
        LOGGER->log_message(LogManager::INFO, "Done with setting the final diff ");

    }*/

    // Autoalign should be fixed or removed 
    /*void autoalignBc0(std::vector<int> selectedTowers) {
        json bc0s_all_towers;
        json diffs;
        std::vector<int> excludedValues;
        int totChannels = 0;
        int maxDelayInBCP = 15;
        for (auto tower: selectedTowers) {
            towers.at(tower)->resetBc0Align();
            sleep(1);
            towers.at(tower)->getBc0ScanList();
            bc0s_all_towers[tower] = towers.at(tower)->jsonifyBc0Scan();
            totChannels += 25;
        }
        LOGGER->log_message(LogManager::INFO, bc0s_all_towers.dump());
        for (auto &tower: bc0s_all_towers.items()) {
            diffs[tower.key()] = {};
            for (int ch = 0; ch<25; ch++) {
              diffs[tower.key()][std::to_string(ch)] = -1;
            }
        }
        LOGGER->log_message(LogManager::INFO, "after init diffs");
        int counter = 0;
        int maxValue = 0;
        bool allowSetDiff = false;
        while (1) {
          maxValue = getBc0Diff(bc0s_all_towers, diffs, excludedValues);
          for (auto &tower: diffs.items()) {
              for (int ch = 0; ch<25; ch++) {
                  if (diffs[tower.key()][std::to_string(ch)] > maxDelayInBCP) counter ++;
              }
          }
          LOGGER->log_message(LogManager::INFO, diffs.dump());
          // The number of channels that cannot use this delay is low. 
          // We can break the loop
          if ( (float(totChannels-excludedValues.size()-counter)/float(totChannels)) * 100 > 70 ) {
              allowSetDiff = true;
              break;
          }
          // Not a good diff, repeat the cycle
          counter = 0;
          excludedValues.push_back(maxValue);
          LOGGER->log_message(LogManager::INFO, "excludedValues");
          for (auto val : excludedValues) LOGGER->log_message(LogManager::INFO, stdsprintf("ExcludedValue 0x%d", val)); 
          if (excludedValues.size() == totChannels/2) { 
              LOGGER->log_message(LogManager::ERROR, " ERROR finding the common bc0 value. Too many bad channels!!! ");
              break;
          }
        } 
        if (allowSetDiff) {
            for (auto tower: selectedTowers) {
                towers.at(tower)->setBc0Align(diffs[tower]);
            }
            LOGGER->log_message(LogManager::INFO, "Done with setting the final diff ");
        }
        LOGGER->log_message(LogManager::INFO, "Done autoalignBc0");
    }*/
};
#endif
