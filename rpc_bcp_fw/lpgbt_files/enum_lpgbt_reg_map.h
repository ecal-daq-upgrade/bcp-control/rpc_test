#ifndef LPGBTMAP_H_
#define LPGBTMAP_H_
enum LpgbtRegisterMap {
CHIPID0=0x0,
CHIPID1=0x1,
CHIPID2=0x2,
CHIPID3=0x3,
USERID0=0x4,
USERID1=0x5,
USERID2=0x6,
USERID3=0x7,
DACCAL0=0x8,
DACCAL1=0x9,
DACCAL2=0xa,
ADCCAL0=0xb,
ADCCAL1=0xc,
ADCCAL2=0xd,
ADCCAL3=0xe,
ADCCAL4=0xf,
ADCCAL5=0x10,
ADCCAL6=0x11,
ADCCAL7=0x12,
ADCCAL8=0x13,
ADCCAL9=0x14,
ADCCAL10=0x15,
ADCCAL11=0x16,
ADCCAL12=0x17,
ADCCAL13=0x18,
ADCCAL14=0x19,
TEMPCALH=0x1a,
TEMPCALL=0x1b,
VREFCNTR=0x1c,
VREFTUNE=0x1d,
CURDACCALH=0x1e,
CURDACCALL=0x1f,
CLKGCONFIG0=0x20,
CLKGCONFIG1=0x21,
CLKGPLLRES=0x22,
CLKGPLLINTCUR=0x23,
CLKGPLLPROPCUR=0x24,
CLKGCDRPROPCUR=0x25,
CLKGCDRINTCUR=0x26,
CLKGCDRFFPROPCUR=0x27,
CLKGFLLINTCUR=0x28,
CLKGFFCAP=0x29,
CLKGCNTOVERRIDE=0x2a,
CLKGOVERRIDECAPBANK=0x2b,
CLKGWAITTIME=0x2c,
CLKGLFCONFIG0=0x2d,
CLKGLFCONFIG1=0x2e,
FAMAXHEADERFOUNDCOUNT=0x2f,
FAMAXHEADERFOUNDCOUNTAFTERNF=0x30,
FAMAXHEADERNOTFOUNDCOUNT=0x31,
RESERVED1=0x32,
PSDLLCONFIG=0x33,
RESERVED2=0x34,
FORCEENABLE=0x35,
CHIPCONFIG=0x36,
EQCONFIG=0x37,
EQRES=0x38,
LDCONFIGH=0x39,
LDCONFIGL=0x3a,
REFCLK=0x3b,
SCCONFIG=0x3c,
RESETCONFIG=0x3d,
PGCONFIG=0x3e,
I2CMTRANSCONFIG=0x3f,
I2CMTRANSADDRESS=0x40,
I2CMTRANSCTRL=0x41,
I2CMTRANSDATA0=0x42,
I2CMTRANSDATA1=0x43,
I2CMTRANSDATA2=0x44,
I2CMTRANSDATA3=0x45,
I2CMTRANSDATA4=0x46,
I2CMTRANSDATA5=0x47,
I2CMTRANSDATA6=0x48,
I2CMTRANSDATA7=0x49,
I2CMTRANSDATA8=0x4a,
I2CMTRANSDATA9=0x4b,
I2CMTRANSDATA10=0x4c,
I2CMTRANSDATA11=0x4d,
I2CMTRANSDATA12=0x4e,
I2CMTRANSDATA13=0x4f,
I2CMTRANSDATA14=0x50,
I2CMTRANSDATA15=0x51,
I2CMCLKDISABLE=0x52,
PIODIRH=0x53,
PIODIRL=0x54,
PIOOUTH=0x55,
PIOOUTL=0x56,
PIOPULLENAH=0x57,
PIOPULLENAL=0x58,
PIOUPDOWNH=0x59,
PIOUPDOWNL=0x5a,
PIODRIVESTRENGTHH=0x5b,
PIODRIVESTRENGTHL=0x5c,
PS0CONFIG=0x5d,
PS0DELAY=0x5e,
PS0OUTDRIVER=0x5f,
PS1CONFIG=0x60,
PS1DELAY=0x61,
PS1OUTDRIVER=0x62,
PS2CONFIG=0x63,
PS2DELAY=0x64,
PS2OUTDRIVER=0x65,
PS3CONFIG=0x66,
PS3DELAY=0x67,
PS3OUTDRIVER=0x68,
PSLOWRES=0x69,
DACCONFIGH=0x6a,
DACCONFIGL=0x6b,
CURDACVALUE=0x6c,
CURDACCHN=0x6d,
EPCLK0CHNCNTRH=0x6e,
EPCLK0CHNCNTRL=0x6f,
EPCLK1CHNCNTRH=0x70,
EPCLK1CHNCNTRL=0x71,
EPCLK2CHNCNTRH=0x72,
EPCLK2CHNCNTRL=0x73,
EPCLK3CHNCNTRH=0x74,
EPCLK3CHNCNTRL=0x75,
EPCLK4CHNCNTRH=0x76,
EPCLK4CHNCNTRL=0x77,
EPCLK5CHNCNTRH=0x78,
EPCLK5CHNCNTRL=0x79,
EPCLK6CHNCNTRH=0x7a,
EPCLK6CHNCNTRL=0x7b,
EPCLK7CHNCNTRH=0x7c,
EPCLK7CHNCNTRL=0x7d,
EPCLK8CHNCNTRH=0x7e,
EPCLK8CHNCNTRL=0x7f,
EPCLK9CHNCNTRH=0x80,
EPCLK9CHNCNTRL=0x81,
EPCLK10CHNCNTRH=0x82,
EPCLK10CHNCNTRL=0x83,
EPCLK11CHNCNTRH=0x84,
EPCLK11CHNCNTRL=0x85,
EPCLK12CHNCNTRH=0x86,
EPCLK12CHNCNTRL=0x87,
EPCLK13CHNCNTRH=0x88,
EPCLK13CHNCNTRL=0x89,
EPCLK14CHNCNTRH=0x8a,
EPCLK14CHNCNTRL=0x8b,
EPCLK15CHNCNTRH=0x8c,
EPCLK15CHNCNTRL=0x8d,
EPCLK16CHNCNTRH=0x8e,
EPCLK16CHNCNTRL=0x8f,
EPCLK17CHNCNTRH=0x90,
EPCLK17CHNCNTRL=0x91,
EPCLK18CHNCNTRH=0x92,
EPCLK18CHNCNTRL=0x93,
EPCLK19CHNCNTRH=0x94,
EPCLK19CHNCNTRL=0x95,
EPCLK20CHNCNTRH=0x96,
EPCLK20CHNCNTRL=0x97,
EPCLK21CHNCNTRH=0x98,
EPCLK21CHNCNTRL=0x99,
EPCLK22CHNCNTRH=0x9a,
EPCLK22CHNCNTRL=0x9b,
EPCLK23CHNCNTRH=0x9c,
EPCLK23CHNCNTRL=0x9d,
EPCLK24CHNCNTRH=0x9e,
EPCLK24CHNCNTRL=0x9f,
EPCLK25CHNCNTRH=0xa0,
EPCLK25CHNCNTRL=0xa1,
EPCLK26CHNCNTRH=0xa2,
EPCLK26CHNCNTRL=0xa3,
EPCLK27CHNCNTRH=0xa4,
EPCLK27CHNCNTRL=0xa5,
EPCLK28CHNCNTRH=0xa6,
EPCLK28CHNCNTRL=0xa7,
EPTXDATARATE=0xa8,
EPTXCONTROL=0xa9,
EPTX10ENABLE=0xaa,
EPTX32ENABLE=0xab,
EPTXECCHNCNTR=0xac,
EPTXECCHNCNTR2=0xad,
EPTX00CHNCNTR=0xae,
EPTX01CHNCNTR=0xaf,
EPTX02CHNCNTR=0xb0,
EPTX03CHNCNTR=0xb1,
EPTX10CHNCNTR=0xb2,
EPTX11CHNCNTR=0xb3,
EPTX12CHNCNTR=0xb4,
EPTX13CHNCNTR=0xb5,
EPTX20CHNCNTR=0xb6,
EPTX21CHNCNTR=0xb7,
EPTX22CHNCNTR=0xb8,
EPTX23CHNCNTR=0xb9,
EPTX30CHNCNTR=0xba,
EPTX31CHNCNTR=0xbb,
EPTX32CHNCNTR=0xbc,
EPTX33CHNCNTR=0xbd,
EPTX01_00CHNCNTR=0xbe,
EPTX03_02CHNCNTR=0xbf,
EPTX11_10CHNCNTR=0xc0,
EPTX13_12CHNCNTR=0xc1,
EPTX21_20CHNCNTR=0xc2,
EPTX23_22CHNCNTR=0xc3,
EPTX31_30CHNCNTR=0xc4,
EPTX33_32CHNCNTR=0xc5,
EPTXLOWRES0=0xc6,
EPTXLOWRES1=0xc7,
EPRX0CONTROL=0xc8,
EPRX1CONTROL=0xc9,
EPRX2CONTROL=0xca,
EPRX3CONTROL=0xcb,
EPRX4CONTROL=0xcc,
EPRX5CONTROL=0xcd,
EPRX6CONTROL=0xce,
EPRXECCONTROL=0xcf,
EPRX00CHNCNTR=0xd0,
EPRX01CHNCNTR=0xd1,
EPRX02CHNCNTR=0xd2,
EPRX03CHNCNTR=0xd3,
EPRX10CHNCNTR=0xd4,
EPRX11CHNCNTR=0xd5,
EPRX12CHNCNTR=0xd6,
EPRX13CHNCNTR=0xd7,
EPRX20CHNCNTR=0xd8,
EPRX21CHNCNTR=0xd9,
EPRX22CHNCNTR=0xda,
EPRX23CHNCNTR=0xdb,
EPRX30CHNCNTR=0xdc,
EPRX31CHNCNTR=0xdd,
EPRX32CHNCNTR=0xde,
EPRX33CHNCNTR=0xdf,
EPRX40CHNCNTR=0xe0,
EPRX41CHNCNTR=0xe1,
EPRX42CHNCNTR=0xe2,
EPRX43CHNCNTR=0xe3,
EPRX50CHNCNTR=0xe4,
EPRX51CHNCNTR=0xe5,
EPRX52CHNCNTR=0xe6,
EPRX53CHNCNTR=0xe7,
EPRX60CHNCNTR=0xe8,
EPRX61CHNCNTR=0xe9,
EPRX62CHNCNTR=0xea,
EPRX63CHNCNTR=0xeb,
EPRXECCHNCNTR=0xec,
EPRXEQ10CONTROL=0xed,
EPRXEQ32CONTROL=0xee,
EPRXEQ54CONTROL=0xef,
EPRXEQ6CONTROL=0xf0,
EPRXDLLCONFIG=0xf1,
EPRXLOCKFILTER=0xf2,
EPRXLOCKFILTER2=0xf3,
RESERVED4=0xf4,
RESERVED5=0xf5,
RESERVED6=0xf6,
READY=0xf7,
WATCHDOG=0xf8,
POWERUP0=0xf9,
POWERUP1=0xfa,
POWERUP2=0xfb,
CRC0=0xfc,
CRC1=0xfd,
CRC2=0xfe,
CRC3=0xff,
I2CM0CONFIG=0x100,
I2CM0ADDRESS=0x101,
I2CM0DATA0=0x102,
I2CM0DATA1=0x103,
I2CM0DATA2=0x104,
I2CM0DATA3=0x105,
I2CM0CMD=0x106,
I2CM1CONFIG=0x107,
I2CM1ADDRESS=0x108,
I2CM1DATA0=0x109,
I2CM1DATA1=0x10a,
I2CM1DATA2=0x10b,
I2CM1DATA3=0x10c,
I2CM1CMD=0x10d,
I2CM2CONFIG=0x10e,
I2CM2ADDRESS=0x10f,
I2CM2DATA0=0x110,
I2CM2DATA1=0x111,
I2CM2DATA2=0x112,
I2CM2DATA3=0x113,
I2CM2CMD=0x114,
EPRXTRAIN10=0x115,
EPRXTRAIN32=0x116,
EPRXTRAIN54=0x117,
EPRXTRAINEC6=0x118,
FUSECONTROL=0x119,
FUSEBLOWDATAA=0x11a,
FUSEBLOWDATAB=0x11b,
FUSEBLOWDATAC=0x11c,
FUSEBLOWDATAD=0x11d,
FUSEBLOWADDH=0x11e,
FUSEBLOWADDL=0x11f,
FUSEMAGIC=0x120,
ADCSELECT=0x121,
ADCMON=0x122,
ADCCONFIG=0x123,
EOMCONFIGH=0x124,
EOMCONFIGL=0x125,
EOMVOFSEL=0x126,
PROCESSANDSEUMONITOR=0x127,
ULDATASOURCE0=0x128,
ULDATASOURCE1=0x129,
ULDATASOURCE2=0x12a,
ULDATASOURCE3=0x12b,
ULDATASOURCE4=0x12c,
ULDATASOURCE5=0x12d,
DPDATAPATTERN3=0x12e,
DPDATAPATTERN2=0x12f,
DPDATAPATTERN1=0x130,
DPDATAPATTERN0=0x131,
EPRXPRBS3=0x132,
EPRXPRBS2=0x133,
EPRXPRBS1=0x134,
EPRXPRBS0=0x135,
BERTSOURCE=0x136,
BERTCONFIG=0x137,
BERTDATAPATTERN3=0x138,
BERTDATAPATTERN2=0x139,
BERTDATAPATTERN1=0x13a,
BERTDATAPATTERN0=0x13b,
RST0=0x13c,
RST1=0x13d,
RST2=0x13e,
POWERUP3=0x13f,
POWERUP4=0x140,
CLKTREE=0x141,
DATAPATH=0x142,
TO0SEL=0x143,
TO1SEL=0x144,
TO2SEL=0x145,
TO3SEL=0x146,
TO4SEL=0x147,
TO5SEL=0x148,
TODRIVINGSTRENGTH=0x149,
TO4DRIVER=0x14a,
TO5DRIVER=0x14b,
TOPREEMP=0x14c,
RESERVED10=0x14d,
RESERVED11=0x14e,
RESERVED12=0x14f,
CONFIGPINS=0x150,
I2CSLAVEADDRESS=0x151,
EPRX0LOCKED=0x152,
EPRX0CURRENTPHASE10=0x153,
EPRX0CURRENTPHASE32=0x154,
EPRX1LOCKED=0x155,
EPRX1CURRENTPHASE10=0x156,
EPRX1CURRENTPHASE32=0x157,
EPRX2LOCKED=0x158,
EPRX2CURRENTPHASE10=0x159,
EPRX2CURRENTPHASE32=0x15a,
EPRX3LOCKED=0x15b,
EPRX3CURRENTPHASE10=0x15c,
EPRX3CURRENTPHASE32=0x15d,
EPRX4LOCKED=0x15e,
EPRX4CURRENTPHASE10=0x15f,
EPRX4CURRENTPHASE32=0x160,
EPRX5LOCKED=0x161,
EPRX5CURRENTPHASE10=0x162,
EPRX5CURRENTPHASE32=0x163,
EPRX6LOCKED=0x164,
EPRX6CURRENTPHASE10=0x165,
EPRX6CURRENTPHASE32=0x166,
EPRXECCURRENTPHASE=0x167,
EPRX0DLLSTATUS=0x168,
EPRX1DLLSTATUS=0x169,
EPRX2DLLSTATUS=0x16a,
EPRX3DLLSTATUS=0x16b,
EPRX4DLLSTATUS=0x16c,
EPRX5DLLSTATUS=0x16d,
EPRX6DLLSTATUS=0x16e,
I2CM0CTRL=0x16f,
I2CM0MASK=0x170,
I2CM0STATUS=0x171,
I2CM0TRANCNT=0x172,
I2CM0READBYTE=0x173,
I2CM0READ0=0x174,
I2CM0READ1=0x175,
I2CM0READ2=0x176,
I2CM0READ3=0x177,
I2CM0READ4=0x178,
I2CM0READ5=0x179,
I2CM0READ6=0x17a,
I2CM0READ7=0x17b,
I2CM0READ8=0x17c,
I2CM0READ9=0x17d,
I2CM0READ10=0x17e,
I2CM0READ11=0x17f,
I2CM0READ12=0x180,
I2CM0READ13=0x181,
I2CM0READ14=0x182,
I2CM0READ15=0x183,
I2CM1CTRL=0x184,
I2CM1MASK=0x185,
I2CM1STATUS=0x186,
I2CM1TRANCNT=0x187,
I2CM1READBYTE=0x188,
I2CM1READ0=0x189,
I2CM1READ1=0x18a,
I2CM1READ2=0x18b,
I2CM1READ3=0x18c,
I2CM1READ4=0x18d,
I2CM1READ5=0x18e,
I2CM1READ6=0x18f,
I2CM1READ7=0x190,
I2CM1READ8=0x191,
I2CM1READ9=0x192,
I2CM1READ10=0x193,
I2CM1READ11=0x194,
I2CM1READ12=0x195,
I2CM1READ13=0x196,
I2CM1READ14=0x197,
I2CM1READ15=0x198,
I2CM2CTRL=0x199,
I2CM2MASK=0x19a,
I2CM2STATUS=0x19b,
I2CM2TRANCNT=0x19c,
I2CM2READBYTE=0x19d,
I2CM2READ0=0x19e,
I2CM2READ1=0x19f,
I2CM2READ2=0x1a0,
I2CM2READ3=0x1a1,
I2CM2READ4=0x1a2,
I2CM2READ5=0x1a3,
I2CM2READ6=0x1a4,
I2CM2READ7=0x1a5,
I2CM2READ8=0x1a6,
I2CM2READ9=0x1a7,
I2CM2READ10=0x1a8,
I2CM2READ11=0x1a9,
I2CM2READ12=0x1aa,
I2CM2READ13=0x1ab,
I2CM2READ14=0x1ac,
I2CM2READ15=0x1ad,
PSSTATUS=0x1ae,
PIOINH=0x1af,
PIOINL=0x1b0,
FUSESTATUS=0x1b1,
FUSEVALUESA=0x1b2,
FUSEVALUESB=0x1b3,
FUSEVALUESC=0x1b4,
FUSEVALUESD=0x1b5,
PROCESSMONITORSTATUS=0x1b6,
PMFREQA=0x1b7,
PMFREQB=0x1b8,
PMFREQC=0x1b9,
SEUCOUNTH=0x1ba,
SEUCOUNTL=0x1bb,
CLKGSTATUS0=0x1bc,
CLKGSTATUS1=0x1bd,
CLKGSTATUS2=0x1be,
CLKGSTATUS3=0x1bf,
CLKGSTATUS4=0x1c0,
CLKGSTATUS5=0x1c1,
CLKGSTATUS6=0x1c2,
CLKGSTATUS7=0x1c3,
CLKGSTATUS8=0x1c4,
CLKGSTATUS9=0x1c5,
DLDPFECCORRECTIONCOUNT0=0x1c6,
DLDPFECCORRECTIONCOUNT1=0x1c7,
DLDPFECCORRECTIONCOUNT2=0x1c8,
DLDPFECCORRECTIONCOUNT3=0x1c9,
ADCSTATUSH=0x1ca,
ADCSTATUSL=0x1cb,
EOMSTATUS=0x1cc,
EOMCOUTERVALUEH=0x1cd,
EOMCOUTERVALUEL=0x1ce,
EOMCOUNTER40MH=0x1cf,
EOMCOUNTER40ML=0x1d0,
BERTSTATUS=0x1d1,
BERTRESULT4=0x1d2,
BERTRESULT3=0x1d3,
BERTRESULT2=0x1d4,
BERTRESULT1=0x1d5,
BERTRESULT0=0x1d6,
ROM=0x1d7,
PORBOR=0x1d8,
PUSMSTATUS=0x1d9,
PUSMPLLWATCHDOG=0x1da,
PUSMDLLWATCHDOG=0x1db,
PUSMCSUMWATCHDOG=0x1dc,
PUSMBROWNOUTWATCHDOG=0x1dd,
PUSMPLLTIMEOUT=0x1de,
PUSMDLLTIMEOUT=0x1df,
PUSMCHANNELSTIMEOUT=0x1e0,
CRCVALUE0=0x1e1,
CRCVALUE1=0x1e2,
CRCVALUE2=0x1e3,
CRCVALUE3=0x1e4,
FAILEDCRC=0x1e5,
TOVALUE=0x1e6,
SCSTATUS=0x1e7,
FASTATE=0x1e8,
FAHEADERFOUNDCOUNT=0x1e9,
FAHEADERNOTFOUNDCOUNT=0x1ea,
FALOSSOFLOCKCOUNT=0x1eb,
CONFIGERRORCOUNTERH=0x1ec,
CONFIGERRORCOUNTERL=0x1ed
};

enum I2cmCommand {
        WRITE_CRA = 0x0,
        WRITE_MSK = 0x1,
        ONE_BYTE_WRITE = 0x2,
        ONE_BYTE_READ = 0x3,
        ONE_BYTE_WRITE_EXT = 0x4,
        ONE_BYTE_READ_EXT = 0x5,
        ONE_BYTE_RMW_OR = 0x6,
        ONE_BYTE_RMW_XOR = 0x7,
        W_MULTI_4BYTE0 = 0x8,
        W_MULTI_4BYTE1 = 0x9,
        W_MULTI_4BYTE2 = 0xA,
        W_MULTI_4BYTE3 = 0xB,
        WRITE_MULTI = 0xC,
        READ_MULTI = 0xD,
        WRITE_MULTI_EXT = 0xE,
        READ_MULTI_EXT = 0xF
};
#endif
