#ifndef TOWER_H
#define TOWER_H

#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <functional>

#include <exception>
#include <stdexcept>

#include "json.hpp"
#include "fw.h"
#include "basic_functions.h"
#include "lpgbt_files/enum_lpgbt_reg_map.h"
#include "Channel.h"

using json = nlohmann::json;
using namespace std::placeholders;

void handle_eptr(std::exception_ptr eptr) // passing by value is ok
{
    try {
        if (eptr) {
            std::rethrow_exception(eptr);
        }
    } catch(const std::exception& e) {
        std::cout << "Caught exception \"" << e.what() << "\"\n";
    }
}

class Tower {
public:
    Tower(LogManager* outlogger, Fw *fwptr, int tower_num) {
        LOGGER = outlogger;
        tower_number = tower_num;
        tower_in_group = tower_number%3;
        tower_group = int(tower_number)/3;
        fwpointer = fwptr;
        fcmd_ctrl << stdsprintf("/BCP_Lpgbt/tg%d/t%d/fast_cmd_group/fcmd_ctrl", tower_group, tower_in_group);

        monit_module_regs = {"corrupted_header", "packet_counter_max", "packet_counter_min", "sample_error_counter", "frame_error_counter", "crc_error_counter", "parity_error_counter"};
        decom_module_regs = {"bc0_c", "wr_data_count_max", "rd_data_count_min", "orbit_sample_err"};
        for (int i=0; i<25; i++) bc0list.push_back(-1);
        
	// We start with all channels enabled
        for (int i=0; i<25; i++) {
          enabled_channels[i] = true;
	}

        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
              channels.push_back(new Channel(LOGGER, fwpointer, tower_number, vfen, chn));
            }
        }

        //LOGGER->log_message(LogManager::INFO, stdsprintf("Created tower %d", tower_number));
    }

    LogManager* LOGGER;
    Fw *fwpointer;
    int tower_number;
    int tower_in_group;
    int tower_group;
    std::ostringstream fcmd_ctrl;
    int VFE_first_add = 8;
    int VFE_ch_step = 8;
    std::vector<Channel*> channels;
    std::vector<std::string> monit_module_regs;
    std::vector<std::string> decom_module_regs;
    std::vector<int> bc0list;
    std::map<int,bool> enabled_channels;
    int bc0AlignPoint = 0;

    void setBc0AlignPoint(int value) {
      bc0AlignPoint = value;
    }


    void configureLpgbtFromFile(int lpgbt_addr, std::string config_file) {
        LOGGER->log_message(LogManager::INFO, "Write configuration from file " + config_file);
        std::ifstream infile(config_file);
        std::string line;
        int add, val;
        while (std::getline(infile, line)) {
            if (line.rfind("#", 0) == 0) continue; // Found a comment
            std::istringstream iss(line);
            if (!(iss >> std::hex >> add >> val)) break;
            writeLpgbt(fwpointer, tower_number, lpgbt_addr, add, val, lpgbt_addr== 113);
        }
    }
   
    void configureLpgbtFromJson(int lpgbt_addr, json lpgbt_json) {
        LOGGER->log_message(LogManager::INFO, "Write configuration from json");
        for (json::iterator it = lpgbt_json.begin(); it != lpgbt_json.end(); ++it) {
            int myvalue = lpgbt_json[it.key()];
            writeLpgbt(fwpointer, tower_number, lpgbt_addr, std::stoi(it.key()), myvalue, lpgbt_addr== 113);
        }
    }
 
    std::string get_tclink_status(bool master_only=false) {
      std::string mystring = "";
      int max_range = 4;
      if (master_only) max_range = 1;
      for(int i=0; i<max_range; i++) {
        std::ostringstream reg_string;
        reg_string << stdsprintf("/BCP_Lpgbt/tg%d/t%d/", tower_group, tower_in_group);
        if (i == 0) reg_string << "master/";
        else reg_string << "slave" << i <<"/";
        uint32_t status1 = readFPGA(fwpointer, reg_string.str()+"rx_frame_locked");
        uint32_t status2 = readFPGA(fwpointer, reg_string.str()+"rx_data_not_idle");
        uint32_t status3 = readFPGA(fwpointer, reg_string.str()+"rx_user_data_ready");
        uint32_t status4 = readFPGA(fwpointer, reg_string.str()+"tx_user_data_ready");
	std::string temp_string = stdsprintf("%s [%d,%d,%d,%d]", reg_string.str().c_str(), status1, status2, status3, status4);
        LOGGER->log_message(LogManager::INFO, temp_string);
	mystring = mystring +"\n" +  temp_string;
      }
     return mystring;
    }
  
    
    void enableVTRxChannels(int vtrx_addr) {
      // Get first VTRx version
      // --> readVTRx(Fw *myfw, int tower, int vtrx_device_add, int vtrx_reg_
      int data = readVTRx(fwpointer, tower_number, vtrx_addr, 0x15);
      int revid = data & 0xF;
      int chipid = data >> 4;
      LOGGER->log_message(LogManager::INFO, stdsprintf("VTRx revid %d", revid));
      LOGGER->log_message(LogManager::INFO, stdsprintf("VTRx chipid %d", chipid));
      if (revid == 5 && chipid == 1) {
        writeVTRx(fwpointer, tower_number, vtrx_addr, 0x0, 0xf); 
      }
    }
 
    std::string reset_tclinks(bool master_only=false) {
      //status_list = get_tclink_status(myBcp, hr=False, master_only=master_only, tower=tower);
      get_tclink_status(master_only=master_only); 
      LOGGER->log_message(LogManager::INFO, "Reset tclink");
      int max_range = 4;
      if (master_only) max_range = 1;
      for(int i=0; i<max_range; i++) {
        std::ostringstream reg_string;
        reg_string << stdsprintf("/BCP_Lpgbt/tg%d/t%d/", tower_group, tower_in_group);
        if (i == 0) reg_string << "master/";
        else reg_string << "slave" << i <<"/";
        writeFPGA(fwpointer,reg_string.str()+"mgt_reset_tx_pll_and_datapath", 0);
        writeFPGA(fwpointer,reg_string.str()+"mgt_reset_all", 0);
        writeFPGA(fwpointer,reg_string.str()+"mgt_reset_tx_datapath", 0);
        writeFPGA(fwpointer,reg_string.str()+"mgt_reset_rx_datapath", 0);
        if (master_only) {
          writeFPGA(fwpointer,reg_string.str()+"mgt_reset_tx_pll_and_datapath", 1);
          writeFPGA(fwpointer,reg_string.str()+"mgt_reset_tx_pll_and_datapath", 0);
          sleep(1); // 19/11/2024 At 1/2 sec, we have EC timeout error occasionally
		    // Set this value to 1 second, we do not see the problem so far 
        }
        writeFPGA(fwpointer,reg_string.str()+"mgt_reset_rx_datapath", 1);
        writeFPGA(fwpointer,reg_string.str()+"mgt_reset_rx_datapath", 0);
      }
      return get_tclink_status();
    }

    void lpgbt_config_done(int lpgbt_addr) {
      LOGGER->log_message(LogManager::INFO, "In lpgbt_config_done");
      if (lpgbt_addr == 113) {
          writeLpgbt(fwpointer, tower_number, lpgbt_addr, LpgbtRegisterMap::POWERUP2, 0x6);
      } else {
          writeLpgbt(fwpointer, tower_number, lpgbt_addr, LpgbtRegisterMap::POWERUP2, 0x6, false);
      }
    }

    void reset_slave_lpgbts() {
        LOGGER->log_message(LogManager::INFO, "Resetting the slave LpGBTs setting to low the RSTB pin");
        LOGGER->log_message(LogManager::INFO, "To do that we need to set to low the GPIO0 of the master lpGBT");
 
	int gpiodirl = readLpgbt(fwpointer, tower_number, 113, LpgbtRegisterMap::PIODIRL);
        LOGGER->log_message(LogManager::INFO, stdsprintf("Reading if GPIOs are input or output: %d", gpiodirl));
	int gpiooutl = readLpgbt(fwpointer, tower_number, 113, LpgbtRegisterMap::PIOOUTL);
        LOGGER->log_message(LogManager::INFO, stdsprintf("Reading output values of GPIOs:", gpiooutl));
 
        LOGGER->log_message(LogManager::INFO, "Setting GPIO0 as output and with value low for 0.01 sec");
        int gpiodirl_new = gpiodirl | 0x1;
        int gpiooutl_new_high = gpiooutl | 0x1;
        int gpiooutl_new_low  = gpiooutl & 0xFE;
	writeLpgbt(fwpointer, tower_number, 113, LpgbtRegisterMap::PIODIRL, gpiodirl_new);
	writeLpgbt(fwpointer, tower_number, 113, LpgbtRegisterMap::PIOOUTL, gpiooutl_new_high);
        //usleep(5000); //FIXME it looks like 5000us are enough
	writeLpgbt(fwpointer, tower_number, 113, LpgbtRegisterMap::PIOOUTL, gpiooutl_new_low);
        //usleep(5000); //FIXME it looks like 5000us are enough
	//We have investigated these sleeps and it now appears they are not necessary, at least on testing 5-6 times
	writeLpgbt(fwpointer, tower_number, 113, LpgbtRegisterMap::PIOOUTL, gpiooutl_new_high);
	writeLpgbt(fwpointer, tower_number, 113, LpgbtRegisterMap::PIODIRL, gpiodirl);
        LOGGER->log_message(LogManager::INFO, "... and back to original value");
    }

    std::vector<int> find_slave_lpgbts() {
    int register_address = 0x00;
    std::vector<int> slave_lpgbt_list;
    //print("Scan over master lpGBT to find the slave lpGBTs")
        for (int i=1; i<128; ++i) {
            //LOGGER->log_message(LogManager::INFO, stdsprintf("Addr: %d", i));
		    try {
        	    //readLpgbt(fwpointer, tower_number, i, register_address, false);
                readSlaveLpgbtI2C(fwpointer, tower_number, i, register_address);
                LOGGER->log_message(LogManager::INFO, stdsprintf("Found slpgbt at %d", i));
    		    slave_lpgbt_list.push_back(i);
		    } catch(...) {
		        //No device found at this address, simply skip
            }
    	}
        return slave_lpgbt_list;	
	}

    std::string configureFE(std::string lpgbts_json_string) {

        if (!FALCON) removeAllShifts();
        json lpgts_json;
        if (lpgbts_json_string != "") lpgts_json = json::parse(lpgbts_json_string);
        if (lpgts_json.contains(std::to_string(113))) configureLpgbtFromJson(113, lpgts_json[std::to_string(113)]);
        //else configureLpgbtFromFile(113, "/mnt/nfs/elm_shared/rpc_test/config_files/lpgbts/master_rom_ec_v33.cnf");
        lpgbt_config_done(113);
        if (!FALCON) reset_tclinks(true);
        enableVTRxChannels(0x50);
        reset_slave_lpgbts();
		//usleep(5000); // sleep required, without slave 1 can have problems. 5000 us not enough
		                // But upon investigating, this sleep does not now seem necessary over 5-6 tests
		std::vector<int> slpgbt_list = find_slave_lpgbts();
        for (auto addr : slpgbt_list) {
            //std::string filename;
            //if (addr == 114) {
            //    filename = "/mnt/nfs/elm_shared/rpc_test/config_files/lpgbts/slave_1_v33.cnf";
            //} else {
            //    filename = "/mnt/nfs/elm_shared/rpc_test/config_files/lpgbts/slave_v33.cnf";
            //}
            if (lpgts_json.contains(std::to_string(addr))) configureLpgbtFromJson(addr, lpgts_json[std::to_string(addr)]);
            //else configureLpgbtFromFile(addr, filename);
            if (addr==115) {
                LOGGER->log_message(LogManager::INFO, "Slave LpGBT 115 => Setting output GPIO");
                writeLpgbt(fwpointer, tower_number, addr, 0x054, 0x00, false);
                writeLpgbt(fwpointer, tower_number, addr, 0x053, 0x18, false);
                writeLpgbt(fwpointer, tower_number, addr, 0x055, 0x10, false);
                writeLpgbt(fwpointer, tower_number, addr, 0x056, 0x00, false);
            }
            LOGGER->log_message(LogManager::INFO, stdsprintf("Setting done for slave %d", addr));
            lpgbt_config_done(addr);
        }
        LOGGER->log_message(LogManager::INFO, "Reset all tclinks");
        usleep(50000); // 19/11/2024 Tested at 50000 µs, works well so far. Find the minimum
        if (!FALCON) return reset_tclinks(false);
	return "";
    }

//    void fcmdStrobe() {
//        // stobe fast command sequence for all towers
//        // FIXME -> Better to put it in basic_functions.h ?? 
//        writeFPGA(fwpointer,"/BCP_TCDS/Fast_cmd_strobe", 0);
//        writeFPGA(fwpointer,"/BCP_TCDS/Fast_cmd_strobe", 1);
//        writeFPGA(fwpointer,"/BCP_TCDS/Fast_cmd_strobe", 0);
//    }

    void fcmdCtrlReset() {
        // It is IMPORTANT to call it at the end of every fcmd sequence
        for (int vfen=0; vfen<5; ++vfen) {
            int vfenp=vfen;
            if(!FALCON) vfenp=vfen+1;
	        writeFPGA(fwpointer,fcmd_ctrl.str()+std::to_string(vfenp), 0);  
        }
    }

    void fastResetVFE() {
        // Fast reset sequnce 2 > 3 > 4 > 8 > a
        uint32_t rsync_sequence = 0xa8432;
        fastCmdVFEs(rsync_sequence);
    }

    // Function used to bring the DTUs back to normal mode after 
    // after we have put them in sync mode and done the bit alignement
    void normalMode() {
        uint32_t normal_mode_cmd = 0x6;
        fastCmdVFEs(normal_mode_cmd);
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = (vfen*5)+chn;
                if (!enabled_channels[ch_tot]) continue;
                channels[ch_tot]->writeDevice("catia", 0x05, 1, 0x0F);
                channels[ch_tot]->writeDevice("catia", 0x05, 1, 0xFF);
            }
        }
    }

    void setFastCmd(int cmd) {
        for (int vfen=0; vfen<5; ++vfen) {
            int vfenp=vfen;
            if(!FALCON) vfenp=vfen+1;
            //LOGGER->log_message(LogManager::INFO, fcmd_ctrl.str()+std::to_string(vfenp));
            writeFPGA(fwpointer,fcmd_ctrl.str()+std::to_string(vfenp), 0);
            //int val = readFPGA(fwpointer,fcmd_ctrl.str()+std::to_string(vfenp));
            //LOGGER->log_message(LogManager::INFO, stdsprintf("Fast cmd at zero %d ", val));
            //sleep(1);
            writeFPGA(fwpointer,fcmd_ctrl.str()+std::to_string(vfenp), cmd);
            //sleep(1);
            //val = readFPGA(fwpointer,fcmd_ctrl.str()+std::to_string(vfenp));
            //LOGGER->log_message(LogManager::INFO, stdsprintf("Fast cmd set %d ", val));
        }
        //myBcp.BCP.BCP_TCDS.LEMO_Config.set(0x00030003);
        //writeFPGA(fwpointer,"/BCP_TCDS/LEMO_Config", 0x00030003);
    }

    void fastCmdVFEs(int cmd) {
        setFastCmd(cmd);
        LOGGER->log_message(LogManager::INFO, stdsprintf("Fast cmd %d loaded", cmd));
        fcmdStrobe(fwpointer);
        LOGGER->log_message(LogManager::INFO, "Strobe done");
        fcmdCtrlReset(); //FIXME do we need this function?
        LOGGER->log_message(LogManager::INFO, "Reset fast commands");
    };

    void configureVFEsFromSingleFile(json data) {
	    loadVREFsFromSingleFile(data);
	    LOGGER->log_message(LogManager::INFO, "Before ADC ch loop");
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = (vfen*5)+chn;
                if (!enabled_channels[ch_tot]) continue;
		        json data_channel = data["channels"][std::to_string(ch_tot)];
		        // ### ADC0 ###
                std::vector<int> bulk_data;
                int init_regn = 0;
		        // I prefer to split the loop of the two ADCs in two different loops

		        for(int regn=0; regn<data_channel["adc0"].size(); ++regn) {
                    if (bulk_data.size() == 0) init_regn = regn;
                    bulk_data.push_back(data_channel["adc0"][regn]);
                    if (bulk_data.size() == 10) {
                        channels[ch_tot]->multiWriteDevice("adc0", init_regn, 1, bulk_data);
                        bulk_data.clear();
                    }
                }
                if (bulk_data.size() != 0) {
                    channels[ch_tot]->multiWriteDevice("adc0", init_regn, 1, bulk_data);
                    bulk_data.clear();
                }
		        // ### ADC1 ###
		        bulk_data.clear();
		        init_regn = 0;
                //LOGGER->log_message(LogManager::INFO, stdsprintf("Data size %d", data_channel["adc1"].size()));
		        for(int regn=0; regn<data_channel["adc1"].size(); ++regn) {
                    if (bulk_data.size() == 0) init_regn = regn;
                    bulk_data.push_back(data_channel["adc1"][regn]);
                    if (bulk_data.size() == 10) {
                        channels[ch_tot]->multiWriteDevice("adc1", init_regn, 1, bulk_data);
                        bulk_data.clear();
                    }
                }
                if (bulk_data.size() != 0) {
                    channels[ch_tot]->multiWriteDevice("adc1", init_regn, 1, bulk_data);
                    bulk_data.clear();
                }
            }
        }
        loadDACGainsFromSingleFile(data, tower_number);
	    /*for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = (vfen*5)+chn;
                if (!enabled_channels[ch_tot]) continue;
                json data_channel = data["channels"][std::to_string(ch_tot)];
                std::vector<int> bulk_data;
                int init_regn = 5;
		        // ### DTU ###
                bulk_data.clear();
                init_regn = 0;
//                for(int regn=0; regn<data_channel["dtu"].size(); ++regn) {
                for(int regn=0; regn<9; ++regn) {
		            int tempval = data_channel["dtu"][regn];
		            //LOGGER->log_message(LogManager::INFO,std::to_string(tempval));
                    bulk_data.push_back(data_channel["dtu"][regn]);
                }
                channels[ch_tot]->multiWriteDevice("dtu", 0, 1, bulk_data);
                bulk_data.clear();
                for(int regn=9; regn<21; ++regn) {
                    int tempval = data_channel["dtu"][regn];
                    //LOGGER->log_message(LogManager::INFO,std::to_string(tempval));
                    bulk_data.push_back(data_channel["dtu"][regn]);
                }
                channels[ch_tot]->multiWriteDevice("dtu", 9, 1, bulk_data);
                bulk_data.clear();
	        }
	    }*/
	    LOGGER->log_message(LogManager::INFO, "End");

    };

    void loadVREFsFromSingleFile(json& data, int tower_number=0) {
        LOGGER->log_message(LogManager::INFO, "loadVREFsFromSingleFile");
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = (vfen*5)+chn;
                if (!enabled_channels[ch_tot]) continue;
                json data_channel = data["channels"][std::to_string(ch_tot)];
                int reg6 = data_channel["catia"][0x06];
                //LOGGER->log_message(LogManager::INFO, stdsprintf("Reg 6 %d", reg6));
                channels[ch_tot]->writeDevice("catia", 0x06, 1, reg6);
            }
        }
    }


    // this function should be called after FE has been configured and channels aligned
    void loadVREFsFromFile(std::string jsonstring = "") {
        LOGGER->log_message(LogManager::INFO, "loadVREFsFromFile");
        json data;
        if (jsonstring == "") {
            // The pointer is needed for the parsing to work
            // apparently different threads do not see the ifstream
            std::ifstream* f = new std::ifstream("/mnt/nfs/elm_shared/rpc_test/rpc_bcp_fw/lpgbt_files/vfes_config.json");
            data = json::parse(*f);
            delete f;
        } else {
            data = json::parse(jsonstring);
            LOGGER->log_message(LogManager::INFO, "after parse");
        }
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                //int catia_add = VFE_first_add+3+VFE_ch_step*chn;
                int ch_tot = (vfen*5)+chn;
                if (!enabled_channels[ch_tot]) continue;
                int vref = data["t"+std::to_string(tower_number)]["vref"][ch_tot];
		//int reg_val = data[std::to_string(tower_number)][std::to_string(ch_tot)]["catia"][0x06];
                channels[ch_tot]->writeDevice("catia", 0x06, 1, 0x0F|(vref<<4));
		//channels[ch_tot]->writeDevice("catia", 0x06, 1, reg_val);

            }
        }
    }

    void loadDACGainsFromSingleFile(json data, int tower_number=0) {
        LOGGER->log_message(LogManager::INFO, "loadDACGainsFromSingleFile");
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = (vfen*5)+chn;
                if (!enabled_channels[ch_tot]) continue;
		        json data_channel = data["channels"][std::to_string(ch_tot)];
	            int reg3 = data_channel["catia"][0x03];
		        int reg4 = data_channel["catia"][0x04];
                //LOGGER->log_message(LogManager::INFO, stdsprintf("Reg 3 and 4 %d %d", reg3, reg4));
		        channels[ch_tot]->writeDevice("catia", 0x04, 1, reg4 >> 8);
		        channels[ch_tot]->writeDevice("catia", 0x04, 1, reg4 & 0xFF);
		        channels[ch_tot]->writeDevice("catia", 0x03, 1, reg3 >> 8);
		        channels[ch_tot]->writeDevice("catia", 0x03, 1, reg3 & 0xFF);

            }
        }
        //LOGGER->log_message(LogManager::INFO, "Loaded DAC in all channels");
    }

    void loadDACGainsFromFile(std::string jsonstring = "") {
        LOGGER->log_message(LogManager::INFO, "loadDACGainsFromFile");
        json data;
        if (jsonstring == "") {
            std::ifstream* f = new std::ifstream("/mnt/nfs/elm_shared/rpc_test/rpc_bcp_fw/lpgbt_files/vfes_config.json");
            data = json::parse(*f);
	        delete f;
        } else {
	    data = json::parse(jsonstring);
        }
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = (vfen*5)+chn;
                if (!enabled_channels[ch_tot]) continue;
                int DACval_g10 = data["t"+std::to_string(tower_number)]["catia_dac_g10"][ch_tot];
                int DACval_g1 = data["t"+std::to_string(tower_number)]["catia_dac_g1"][ch_tot];
                //int DACval_tot = data[std::to_string(tower_number)][std::to_string(ch_tot)]["catia"][0x03];
		//int DACval_g10 = (DACval_tot & 0xF) >> 2;
	        //int DACval_g1 = (DACval_tot >> 8);
                channels[ch_tot]->setDACGains(DACval_g1, DACval_g10);
            }
        }
        //LOGGER->log_message(LogManager::INFO, "Loaded DAC in all channels");
    }

    void loadADCsConfigFromFile(std::string jsonstring = "") {
        LOGGER->log_message(LogManager::INFO, "loadADCsConfigFromFile");
        // #using calibration of ADCs from file ADC_config_file_t0_vfe1.txt
        // config_file_name = calib_folder+f"ADC_config_file_bcp1"; # configureALLADCs will add {base_file_name}_{bcpNum}_t{tower}_vfe{vfe_num}{end_file_name}
        // lpgbt_functions.configureAllADCs(myBcp, tower=t, base_file_name = config_file_name)
	    json data;
        if (jsonstring == "") {
            std::ifstream* f = new std::ifstream("/mnt/nfs/elm_shared/rpc_test/rpc_bcp_fw/lpgbt_files/ADCcfg.json");
            data = json::parse(*f);
	    delete f;
	    } else {
            data = json::parse(jsonstring);
        }
        // FIXME promote this variable to something more global...
        int nregs_adcs = 76;
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                //int adc0_add = VFE_first_add+0+VFE_ch_step*chn;        
                //int adc1_add = VFE_first_add+1+VFE_ch_step*chn;
                int ch_tot = vfen*5+chn;
                if (!enabled_channels[ch_tot]) continue;
                // Maybe, would be better to have less nested loops... FIXME 
                std::vector<int> data0, data1;
                int init_regn = 0;
                for(int regn=0; regn<nregs_adcs; ++regn) {
                    if (data0.size() == 0) init_regn = regn;
                    data0.push_back(data["t0"]["adc0"]["ch_"+std::to_string(ch_tot)][regn]);
                    data1.push_back(data["t0"]["adc1"]["ch_"+std::to_string(ch_tot)][regn]);
                    //int val0 = data["t0"]["adc0"]["ch_"+std::to_string(vfen*5+chn)][regn];
                    //int val1 = data["t0"]["adc1"]["ch_"+std::to_string(vfen*5+chn)][regn];
                    if (data0.size() == 10) {
                        //multiWriteVFEDevice(fwpointer, tower_number, vfen, adc0_add, init_regn, 1, data0);
                        //multiWriteVFEDevice(fwpointer, tower_number, vfen, adc1_add, init_regn, 1, data1);
                        //channels[ch_tot]->multiWriteADC0(init_regn, 1, data0);
                        //channels[ch_tot]->multiWriteADC1(init_regn, 1, data1);
                        channels[ch_tot]->multiWriteDevice("adc0", init_regn, 1, data0);
                        channels[ch_tot]->multiWriteDevice("adc1", init_regn, 1, data1);
                        data0.clear();
                        data1.clear();
                    }
                }
                if (data0.size() != 0) {
                    //multiWriteVFEDevice(fwpointer, tower_number, vfen, adc0_add, init_regn, 1, data0);
                    //multiWriteVFEDevice(fwpointer, tower_number, vfen, adc1_add, init_regn, 1, data1);
                    //channels[ch_tot]->multiWriteADC0(init_regn, 1, data0);
                    //channels[ch_tot]->multiWriteADC1(init_regn, 1, data1);
                    channels[ch_tot]->multiWriteDevice("adc0", init_regn, 1, data0);
                    channels[ch_tot]->multiWriteDevice("adc1", init_regn, 1, data1);
                    data0.clear();
                    data1.clear();
                }
            }
        }
        
    };

    void pedestalscan() {
        int ch_tot = 5;
        int DACval_g1 = 32;
        for (int dac_val=0x10; dac_val<0x25; dac_val++) {
                channels[ch_tot]->setDACGains(DACval_g1, dac_val);
                LOGGER->log_message(LogManager::INFO, stdsprintf("Set dacval %d", dac_val));
                sleep(10);
        }
    }

    json getAllVFERegisters() {
        json data;
        data["channels"] = {};
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = (vfen*5)+chn;
                if (!enabled_channels[ch_tot]) continue;
                data["channels"][std::to_string(ch_tot)] = channels[ch_tot]->getAllRegisters();
            }
        }
        return data;
    };

    json getErrorStatus() {
        json data;
        data["channels"] = {};
        for(int i=0; i<25; i++) {
            data["channels"][std::to_string(i)] = {};
            std::ostringstream reg_string;
            reg_string << stdsprintf("/BCP_Lpgbt/tg%d/t%d/ch%d/", tower_group, tower_in_group, i);
            for (auto reg_name : monit_module_regs){
                uint32_t status1 = readFPGA(fwpointer, reg_string.str()+"monit_module/"+reg_name);
                // std::cout<<"register " << reg_name << " = " << status1 << std::endl;
                data["channels"][std::to_string(i)][reg_name] = status1;
            }
            for (auto reg_name : decom_module_regs){
                uint32_t status1 = readFPGA(fwpointer, reg_string.str()+"decomp_module/"+reg_name);
                std::cout<<"register " << reg_name << " = " << status1 << std::endl;
                data["channels"][std::to_string(i)][reg_name] = status1;
            }
        }
        return data;
    }


    json getSingleError(std::string errorname) {
        json data;
        data["channels"] = {};
        uint32_t status1 = 999;
        for(int i=0; i<25; i++) {
            data["channels"][std::to_string(i)] = {};
            std::ostringstream reg_string;
            reg_string << stdsprintf("/BCP_Lpgbt/tg%d/t%d/ch%d/", tower_group, tower_in_group,i);
            int cnt = count(monit_module_regs.begin(), monit_module_regs.end(), errorname);

            if (cnt == 1)
                status1 = readFPGA(fwpointer, reg_string.str()+"monit_module/"+errorname);
            cnt = count(decom_module_regs.begin(), decom_module_regs.end(), errorname);
            if (cnt == 1)
                status1 = readFPGA(fwpointer, reg_string.str()+"decomp_module/"+errorname);
            std::cout<<"register " << errorname << " = " << status1 << std::endl;
            data["channels"][std::to_string(i)][errorname] = status1;
        }
        return data;
    }

    json getErrorList() {

        json errors;

        for(auto err_name : monit_module_regs) {
            errors.push_back(err_name);
        }
        for(auto err_name : decom_module_regs) {
            errors.push_back(err_name);
        }
        return errors;
    }


    /*void getBc0ScanList() {
        for (auto &ch: bc0list) ch = -1;
        for (uint32_t cnt=0; cnt<=1000; cnt++){
            // setting the delay
            std::ostringstream bc0_reg_string;
            bc0_reg_string << stdsprintf("/BCP_TCDS/bc0_scan_register");
            writeFPGA(fwpointer, bc0_reg_string.str(), cnt);

            for(int i=0; i<25; i++) {
                std::ostringstream reg_string;
                reg_string << stdsprintf("/BCP_Lpgbt/tg%d/t%d/ch%d/decomp_module/bc0_c", tower_group, tower_in_group,i);
                uint32_t bc0 = readFPGA(fwpointer, reg_string.str());
                if (bc0 == 1) {
                    bc0list[i] = cnt;
                }
            }
         }
    }*/
    
    void getBc0(int cnt) {
        for(int i=0; i<25; i++) {
            std::ostringstream reg_string;
            reg_string << stdsprintf("/BCP_Lpgbt/tg%d/t%d/ch%d/decomp_module/bc0_c", tower_group, tower_in_group, i);
            uint32_t bc0 = readFPGA(fwpointer, reg_string.str());
            if (bc0 == 1) {
                bc0list[i] = cnt;
            }
        }
    }
    
    json jsonifyBc0Scan() {
        json bc0j;
        for(int i=0; i<25; i++) {
            bc0j[std::to_string(i)] = bc0list.at(i);
        }
        return bc0j;
    }

    void resetBc0Align() {
        for (int ch=0; ch<25; ch++ ) {
            std::ostringstream reg_string;
            reg_string << stdsprintf("/BCP_Lpgbt/tg%d/t%d/ch%d/decomp_module/delay_channel", tower_group, tower_in_group, ch);
            writeFPGA(fwpointer, reg_string.str(), 0x0);
            //uint32_t valueee = readFPGA(fwpointer, reg_string.str());
            //LOGGER->log_message(LogManager::INFO, stdsprintf("In resetBc0Align read value %d (meant to be zero)", valueee)); 
        }
        //usleep(1); // It was 1 second. What is the lower limit?
    }

    void setBc0Align(json diff) { //diff is already the diff calculated between bc0 and desired position!!! 
        for (int ch=0; ch<25; ch++ ) {
            std::ostringstream reg_string;
            reg_string << stdsprintf("/BCP_Lpgbt/tg%d/t%d/ch%d/decomp_module/delay_channel", tower_group, tower_in_group, ch);
            writeFPGA(fwpointer, reg_string.str(), diff[std::to_string(ch)]);
        }
        //usleep(1); // It was 1 second. What is the lower limit?
    }


//    void configureDTU(int vfen, int chn, bool invert_clk) {
/*    template <typename... T>
    void configureDTU(std::tuple<T...> TupleTest) {
        int vfen;
        int chn;
        bool invert_clk;
        std::tie(vfen, chn, invert_clk) = TupleTest;
        int ch_tot = (vfen*5)+chn;
        int dtu_add = VFE_first_add+2+VFE_ch_step*chn;
        std::vector<int> bulk_data{0x81, 0x03, 0x04, 0x20, 0x00, 0x00, 0x00, 0xff, 0x0f};
        //multiWriteVFEDevice(fwpointer, tower_number, vfen, dtu_add, 0x0, 1, bulk_data);
        channels[ch_tot]->multiWriteDTU(0x0, 1, bulk_data);
        if (invert_clk) {
            LOGGER->log_message(LogManager::INFO, "Inverting Clocks of LiTE-DTU...");
            //int val_reg4 = readVFEDevice(fwpointer, tower_number, vfen, dtu_add, 0x04);
            int val_reg4 = channels[ch_tot]->readDTU(0x04, 1);
            // inverts ADCClkIn AND Clock (register 4, Giannis mail 23.02.23)
            //writeVFEDevice(fwpointer, tower_number, vfen, dtu_add, 0x04, 1, val_reg4 | 0x60);
            channels[ch_tot]->writeDTU(0x04, 1, val_reg4 | 0x60);
        }
        std::vector<int> bulk_data_2{0x00, 0x00, 0x88, 0x22, 0x99, 0x99, 0x00, 0x3c, 0x32, 0x88, 0x8f, 0x00}; // with Gianni suggestions
        //multiWriteVFEDevice(fwpointer, tower_number, vfen, dtu_add, 0x9, 1, bulk_data_2);
        channels[ch_tot]->multiWriteDTU(0x9, 1, bulk_data_2);
        LOGGER->log_message(LogManager::INFO, stdsprintf("Configured DTU %d in VFE %d", vfen, chn));
    };

    void togglePLLForce(int vfen, int chn, bool enable) {
        int dtu_add = VFE_first_add+2+VFE_ch_step*chn;
        int reg17_val = 0x34; // Auto PLL Marc value
        int ch_tot = (vfen*5)+chn;
        if (enable) reg17_val = 0x3c; // Manual PLL Marc value
        try {
            std::vector<int> bulk_data;
            //multiReadVFEDevice(fwpointer, tower_number, vfen, dtu_add, 0x09, 1, bulk_data, 9);
            channels[ch_tot]->multiReadDTU(0x9, 1, bulk_data, 9);
            std::stringstream result;
            std::copy(bulk_data.begin(), bulk_data.end(), std::ostream_iterator<int>(result, " "));
            LOGGER->log_message(LogManager::INFO, result.str());
            bulk_data.at(0x11-0x09) = reg17_val;
            //multiWriteVFEDevice(fwpointer, tower_number, vfen, dtu_add, 0x9, 1, bulk_data);
            channels[ch_tot]->multiWriteDTU(0x9, 1, bulk_data);
        } catch(...) {
            LOGGER->log_message(LogManager::INFO, stdsprintf("Failing to set PLL force to %d in vfe %d ch %d", enable, vfen, chn));
        }
    };
    
    void setPLLPhase(int vfen, int chn) {
        // For the moment we use the same pll val for all the channels
        int pll_conf = 57;
        int pll_conf_reg16 = pll_conf & 0xFF;
        int pll_conf_reg15 = (pll_conf >> 8) & 0x1;
        int ch_tot = (vfen*5)+chn;
        //int dtu_add = VFE_first_add+2+VFE_ch_step*chn;
        //TODO: they don't match with configureDTU bulk data
        LOGGER->log_message(LogManager::INFO, "!!! Warning DTU register hardcoded !!!");
        std::vector<int> bulk_data{0,60,136,68,85,85,0,0};
        bulk_data.at(6) = (bulk_data.at(6)&0xFE) | pll_conf_reg15;
        bulk_data.at(7) = pll_conf_reg16;
        //multiWriteVFEDevice(fwpointer, tower_number, vfen, dtu_add, 0x9, 1, bulk_data);
        channels[ch_tot]->multiWriteDTU(0x9, 1, bulk_data);
    };
*/
    void enableSyncMode(bool enable) {
        if (enable) fastCmdVFEs(5); // Sync mode
        else fastCmdVFEs(6); // Normal Mode
    };

    //template<typename Fn, typename... Args>
    /*template <typename... T>
    void applyToAllChannels(std::function<void(std::tuple<T...>)> func, std::tuple<T...> TupleTest) {
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                auto t2 = std::tuple_cat(std::make_tuple(vfen, chn), TupleTest);
                func(t2);
            }
        }
    }*/

    void configureDTUAll(json data, bool invert_clk) {
      std::exception_ptr eptr;
      try {
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = vfen*5+chn;
                if (!enabled_channels[ch_tot]) continue;
                channels[ch_tot]->configureDTU(data["channels"][std::to_string(ch_tot)]["dtu"], invert_clk);
            }
        }
      } catch(...) {
        eptr = std::current_exception(); // capture
      }
      handle_eptr(eptr);
    }

    void togglePLLForceAll(bool enable) {
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = vfen*5+chn;
                if (!enabled_channels[ch_tot]) continue;
                channels[ch_tot]->togglePLLForce(enable);
            }
        }
    }

    void setPLLPhaseAll(json data) {
        for(int vfen=0; vfen<5; ++vfen) {
            for(int chn=0; chn<5; ++chn) {
                int ch_tot = vfen*5+chn;
                if (!enabled_channels[ch_tot]) continue;
                LOGGER->log_message(LogManager::INFO, stdsprintf("setPLLPhaseAll channel %d", ch_tot));
                channels[ch_tot]->setPLLPhaseRegisters(data["channels"][std::to_string(ch_tot)]["dtu"]);
                //channels[ch_tot]->setPLLPhase(data["pll_phases"][ch_tot]);
            }
        }
    }

    // TODO: Hardcoded lpgbts... ok?
    void reinitLpgbtTrain() {
        for (int lpgbt : {113,114,115,116,117}) {
            for (int addr : {0x115,0x116,0x117,0x118}) writeLpgbt(fwpointer, tower_number, lpgbt, addr, 0x11);
            for (int addr : {0x115,0x116,0x117,0x118}) writeLpgbt(fwpointer, tower_number, lpgbt, addr, 0x00);
        }
    };

    void resetPLL() {
      fastCmdVFEs(0xf);
    }


    void extractChannelsAvailable(json config_data) {
        for (int i=0; i<25; i++) {
            if (config_data["channels"].contains(std::to_string(i))) enabled_channels[i] = true;
            else enabled_channels[i] = false;
            LOGGER->log_message(LogManager::INFO, stdsprintf("Channel %d enable: %d", i, enabled_channels[i]));
        }
    }


    // This function should be used to config DTUs to send the test pattern
    // Then the BCP uses it to perform the bit alignement
    // This function is taken from the first part of align_BCP_with_sync_mode
    void prepareDTUalign(json config_data, bool invert_clk=false) {

        extractChannelsAvailable(config_data);
        LOGGER->log_message(LogManager::INFO, "Start prepareDTUalignment");
        LOGGER->log_message(LogManager::INFO, "Slpgbt 115 reset DTUs by GPIO");
        writeLpgbt(fwpointer, tower_number, 115, 0x055, 0x00);
        //sleep(1); //19/11/2024 Appears that this sleep is not necessary
        writeLpgbt(fwpointer, tower_number, 115, 0x055, 0x10);
        //sleep(1);  //19/11/2024 Appears that this sleep is not necessary
        LOGGER->log_message(LogManager::INFO, "Fast reset VFEs");
        fastResetVFE();
        LOGGER->log_message(LogManager::INFO, "Configuring DTUs");
        //usleep(5000); //19/11/2024 Appears that this sleep is not necessary
        configureDTUAll(config_data, invert_clk);
        //usleep(500000); //19/11/2024 Appears that this sleep is not necessary
        LOGGER->log_message(LogManager::INFO, "Reset PLL");
        resetPLL();
        usleep(50000); //19/11/2024 We need this sleep for the pll alignment of certain channels
		      // Minimum looks to be at 50000 µs
		      // When in auto, system needs more time in which 50000 µs is the minimum. In force, we can remove this sleep
        //LOGGER->log_message(LogManager::INFO, "Enabling PLL Force");
        //togglePLLForceAll(true);
        //fastCmdVFEs(0x15); // why this fast comand? not sure maybe dtuv3
        //usleep(5000); //19/11/2024 Appears that this sleep is not necessary
        //LOGGER->log_message(LogManager::INFO, "Setting PLL Phase");
        //setPLLPhaseAll(config_data);
        // Enable normal mode before to allow lpGBT
        // phase scan to find the optimal sampling point
        LOGGER->log_message(LogManager::INFO, "Disabel sync mode");
        enableSyncMode(false);
        reinitLpgbtTrain();
        usleep(10000); // We need this sleep! 
                        // Without it some PLLs in DTUs can break (a few bits jump is done when not expected). 
                        // Tested the lower limit vaguely. Problem appears at 1000 µs but not 5000 µs 
			// Found a misaligned channel when investiating another sleep, moved up to 10000 µs
                        // Many attempts could be necesary to see the error, eg 9/10 are correct.
        if (!FALCON) removeAllShifts();
        enableSyncMode(true);
        LOGGER->log_message(LogManager::INFO, "DTU are now sending test pattern");
    };

    void removeAllShifts() {
        LOGGER->log_message(LogManager::INFO, "Remove all bit shifts");
        for (int chn=0; chn<25; chn++) {
            std::ostringstream reg_string;
            reg_string << stdsprintf("/BCP_Lpgbt/tg%d/t%d/ch%d/up_bitslip_conf_sync", tower_group, tower_in_group, chn);
            writeFPGA(fwpointer, reg_string.str(), 0);           
	}
    }

    
};

#endif
