#ifndef CHANNEL_H
#define CHANNEL_H

#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <functional>

#include "json.hpp"
#include "fw.h"
#include "basic_functions.h"
#include "lpgbt_files/enum_lpgbt_reg_map.h"

using json = nlohmann::json;
using namespace std::placeholders;

class Channel {
public:
    Channel(LogManager* outlogger, Fw *fwptr, int outtower, int outvfen, int outch_in_vfe) {
        LOGGER = outlogger;
        vfen = outvfen;
        ch_in_vfe = outch_in_vfe;
        ch_in_tower = ch_in_vfe+vfen*5;
        fwpointer = fwptr;
        tower = outtower;
    
        chip_map.insert({ "adc0",   VFE_first_add + 0 + ch_in_vfe*VFE_ch_step});
        chip_map.insert({ "adc1",   VFE_first_add + 1 + ch_in_vfe*VFE_ch_step});
        chip_map.insert({ "dtu",    VFE_first_add + 2 + ch_in_vfe*VFE_ch_step});
        chip_map.insert({ "catia",  VFE_first_add + 3 + ch_in_vfe*VFE_ch_step});
        //LOGGER->log_message(LogManager::INFO, stdsprintf("Created channel %d", ch_in_tower));
    }

    LogManager* LOGGER;
    Fw *fwpointer;
    int tower;
    int vfen;
    int ch_in_vfe;
    int ch_in_tower;
    int adc0_add;
    int adc1_add;
    int dtu_add;
    int catia_add;
    int VFE_first_add = 8;
    int VFE_ch_step = 8;
    std::map<std::string, int> chip_map;

    void writeDevice(std::string chip_type, int reg, int reg_width, int value) {
        writeVFEDevice(fwpointer, tower, vfen, chip_map.at(chip_type), reg, reg_width, value);
    }
 
    void multiWriteDevice(std::string chip_type, int reg, int reg_width, std::vector<int>& bulk_data) {
        multiWriteVFEDevice(fwpointer, tower, vfen, chip_map.at(chip_type), reg, reg_width, bulk_data);
    };

    int readDevice(std::string chip_type, int reg, int reg_width) {
        return readVFEDevice(fwpointer, tower, vfen, chip_map.at(chip_type), reg, reg_width);
    };

    void multiReadDevice(std::string chip_type, int reg, int reg_width, std::vector<int>& bulk_data, int nb_bytes=1) {
        multiReadVFEDevice(fwpointer, tower, vfen, chip_map.at(chip_type), reg, reg_width, bulk_data, nb_bytes);
    };


    void setDACGains(int DACval_g1, int DACval_g10) {
        writeDevice("catia", 0x04, 1, 0x0);
        writeDevice("catia", 0x04, 1, 0x0);
        int DACval_tot = DACval_g1 & 0x3f;
        DACval_tot = DACval_tot << 8;
        DACval_tot = DACval_tot + (DACval_g10 << 2);
        DACval_tot = DACval_tot + 3;
        writeDevice("catia", 0x03, 1, DACval_tot>>8);
        writeDevice("catia", 0x03, 1, DACval_tot & 0xFF);
    }


    void togglePLLForce(bool enable) {
        int reg17_val = 0x72; // Auto PLL Marc value
        if (enable) reg17_val = 0x7e; // Manual PLL Marc value
        try {
            std::vector<int> bulk_data;
            multiReadDevice("dtu", 0x9, 1, bulk_data, 9);
            std::stringstream result;
            //std::copy(bulk_data.begin(), bulk_data.end(), std::ostream_iterator<int>(result, " "));
            //LOGGER->log_message(LogManager::INFO, result.str());
            bulk_data.at(0x11-0x09) = reg17_val;
            multiWriteDevice("dtu", 0x9, 1, bulk_data);
        } catch(...) {
            LOGGER->log_message(LogManager::INFO, stdsprintf("Failing to set PLL force to %d in vfe %d ch %d", enable, vfen, ch_in_vfe));
        }
    };

    /*void setPLLPhase(int pll_conf = 999) {
        if (pll_conf == 999) pll_conf = 57;
        int pll_conf_reg16 = pll_conf & 0xFF;
        int pll_conf_reg15 = (pll_conf >> 8) & 0x1;
        //TODO: !!! Warning DTU register hardcoded !!!
        //LOGGER->log_message(LogManager::INFO, "!!! Warning DTU register hardcoded !!!");
        std::vector<int> bulk_data{0,60,136,68,85,85,0,0};
        bulk_data.at(6) = (bulk_data.at(6)&0xFE) | pll_conf_reg15;
        bulk_data.at(7) = pll_conf_reg16;
        multiWriteDevice("dtu", 0x9, 1, bulk_data);
    };*/

    void setPLLPhaseRegisters(json &dtu_registers) {
        //LOGGER->log_message(LogManager::WARNING, "setPLLPhaseRegisters with some hardcoded values!");
        std::vector<int> bulk_data{0,60,136,68,85,85,0,0};
        //std::vector<int> bulk_data;
        //for (json::iterator it = dtu_registers.begin()+9; it < dtu_registers.begin()+17; ++it) {
        //    int val = *it;
        //    bulk_data.push_back(val);
        //}
        int reg15 = dtu_registers[15];
        int reg16 = dtu_registers[16];
        bulk_data.at(6) = reg15;
        bulk_data.at(7) = reg16;
        multiWriteDevice("dtu", 0x9, 1, bulk_data);
    }

    void configureDTU(json &dtu_registers, bool invert_clk=false) {
        //std::vector<int> bulk_data_1a{0x81, 0x03, 0x04, 0x20, 0x00, 0x00, 0x00, 0xff, 0x0f};
        std::vector<int> bulk_data_1;
        int counter = 0;
        for (json::iterator it = dtu_registers.begin(); it < dtu_registers.begin()+9; ++it) {
            int val = *it;
            bulk_data_1.push_back(val);
            //LOGGER->log_message(LogManager::INFO, stdsprintf("channel %d, %d orig %d from json %d", ch_in_tower, counter, bulk_data_1a[counter], bulk_data_1[counter]));
            counter ++;
        }

        multiWriteDevice("dtu", 0x0, 1, bulk_data_1);
        if (invert_clk) {
            int val_reg4 = readDevice("dtu", 0x04, 1);
            writeDevice("dtu", 0x04, 1, val_reg4 | 0x60);
        }

        //std::vector<int> bulk_data_2a{0x00, 0x00, 0x88, 0x22, 0x99, 0x99, 0x00, 0x09, 0x72, 0x88, 0x8f, 0x00};
        std::vector<int> bulk_data_2;
        counter = 0;
        for (json::iterator it = dtu_registers.begin()+9; it < dtu_registers.begin()+21; ++it) {
            int val = *it;
            bulk_data_2.push_back(val);
            //LOGGER->log_message(LogManager::INFO, stdsprintf("channel %d, %d orig %d from json %d", ch_in_tower, counter+9, bulk_data_2a[counter], bulk_data_2[counter]));
            counter ++;
        }
        multiWriteDevice("dtu", 0x9, 1, bulk_data_2);

    };

    json getAllRegisters() {
        json data;
        data["adc0"] = json::array();
        data["adc1"] = json::array();
        data["dtu"] = json::array();
	data["catia"] = json::array();
        // Get ADCs values
        int step = 10;
        int max_reg = 76;
	try {
            for (int reg=0; reg<max_reg; reg += step) {
                std::vector<int> bulk_data0;
                std::vector<int> bulk_data1;
	        multiReadDevice("adc0", reg, 1, bulk_data0, std::min(step, max_reg-reg));
	        multiReadDevice("adc1", reg, 1, bulk_data1, std::min(step, max_reg-reg));
	        for (auto& it : bulk_data0) data["adc0"].push_back(it);
	        for (auto& it : bulk_data1) data["adc1"].push_back(it);
            }
	} catch (...) {
	    LOGGER->log_message(LogManager::ERROR, stdsprintf("Error reading ADCs of channel %d of tower %d", ch_in_tower, tower));
	}
        // Get DTUs values: for the moment we are reading just up to reg 0x14. 
        // 0x15-0x19 are missing. To be added when we will have new DTUs 
        std::vector<int> dtu_bulk_data0;
        std::vector<int> dtu_bulk_data1;
	try {
            multiReadDevice("dtu", 0x0, 1, dtu_bulk_data0, 9);
            multiReadDevice("dtu", 0x9, 1, dtu_bulk_data1, 12);
            for (auto& it : dtu_bulk_data0) data["dtu"].push_back(it);
            for (auto& it : dtu_bulk_data1) data["dtu"].push_back(it);
	} catch (...) {
            LOGGER->log_message(LogManager::ERROR, stdsprintf("Error reading DTU of channel %d of tower %d", ch_in_tower, tower));
	}
        // Get CATIA values:
        // until new CATIA we will read only 255
        std::vector<int> catia_bulk_data;
	try {
            multiReadDevice("catia", 0x0, 1, catia_bulk_data, 7);
            for (auto& it : catia_bulk_data) data["catia"].push_back(it);
	} catch (...) {
	    LOGGER->log_message(LogManager::ERROR, stdsprintf("Error reading CATIA of channel %d of tower %d", ch_in_tower, tower));
	}
        return data;
    }
    
};

#endif
