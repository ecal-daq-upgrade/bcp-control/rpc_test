#ifndef BASIC_FUNCTIONS
#define BASIC_FUNCTIONS

#include <assert.h>
#include "fw.h"
#include <libeasymem.h>
#include <sstream>
#include <stdexcept>
#include <fstream>
#include "lpgbt_files/enum_lpgbt_reg_map.h"
#include <bitset>


void writeLpgbt(Fw *myfw, int tower, int lpgbt_add, int reg_add, int data, bool ec_comm=true);
int readLpgbt(Fw *myfw, int tower, int lpgbt_add, int reg_add, bool ec_comm=true);

//TODO: do we still need this version of readFPGA and writeFPGA?
uint32_t readFPGA(void *memrange, Reg the_reg) {

  uint32_t data[1];

  if (easymem_saferead32(memrange, the_reg.offset, 1, data, 1) == 0) {
    return (data[0] >> the_reg.bitoffset) & ((1 << the_reg.bitsize) - 1);
  }
  else {
    LOGGER->log_message(LogManager::ERROR, stdsprintf("read easymem error: %d", errno));
  }
  return 999;
}

void writeFPGA(void *memrange, Reg the_reg, uint32_t val) {
  uint32_t data[1];
  data[0] =  readFPGA(memrange, the_reg);
  // Here we suppose that we are working with 32 bits registers only
  uint32_t mask = 0xFFFF - ((1 << the_reg.bitsize) - 1);
  data[0] &= mask;
  data[0] |= (val << the_reg.bitoffset);
  if (easymem_safewrite32(memrange, the_reg.offset, 1, data, 1) != 0) {
    LOGGER->log_message(LogManager::ERROR, stdsprintf("write easymem error: errno %d", errno));
  }
}

uint32_t readFPGA(Fw *myfw, std::string reg_name, bool full_reg=false) {

  std::map<std::string, Reg>::iterator it = myfw->reg_map.find(reg_name);
  if (it != myfw->reg_map.end()) {
    Reg the_reg = it->second;
  
    uint32_t data[1];

    if (easymem_saferead32(myfw->getMemPointer(), the_reg.offset, 1, data, 1) == 0) {

        if (!full_reg) return (data[0] >> the_reg.bitoffset) & ((1 << the_reg.bitsize) - 1);
        else return data[0];
    }
    else {
        LOGGER->log_message(LogManager::ERROR, stdsprintf("read easymem error: %d", errno));
        throw std::invalid_argument("read easymem error");
    }
  } else {
     throw std::invalid_argument("Bad register name");
  }
  return 999;
}

void writeFPGA(Fw *myfw, std::string reg_name, uint32_t val) {

  std::map<std::string, Reg>::iterator it = myfw->reg_map.find(reg_name);
  if (it != myfw->reg_map.end()) {
      Reg the_reg = it->second;
      uint32_t data[1];
      //LOGGER->log_message(LogManager::INFO, stdsprintf("%s val 0x%04x", reg_name.c_str(), val));
      data[0] =  readFPGA(myfw, reg_name, true);
      //LOGGER->log_message(LogManager::INFO, stdsprintf("data[0] 0x%04x", data[0]));
      // Here we suppose that we are working with 32 bits registers only
      uint32_t mask = 0xFFFFFFFF - (((1 << the_reg.bitsize) - 1) << the_reg.bitoffset);
      //LOGGER->log_message(LogManager::INFO, stdsprintf("mask 0x%04x", mask));
      data[0] &= mask;
      //LOGGER->log_message(LogManager::INFO, stdsprintf("data[0] dopo mask 0x%04x", data[0]));
      //LOGGER->log_message(LogManager::INFO, stdsprintf("write val FPGA %d", val));
      data[0] |= (val << the_reg.bitoffset);
      //LOGGER->log_message(LogManager::INFO, stdsprintf("data[0] dopo val 0x%04x", data[0]));
      if (easymem_safewrite32(myfw->getMemPointer(), the_reg.offset, 1, data, 1) != 0) {
        LOGGER->log_message(LogManager::ERROR, stdsprintf("write easymem error: errno %d", errno));
        throw std::invalid_argument("write easymem error");
      }
  } else {
     throw std::invalid_argument("Bad register name");
  }
}

void fcmdStrobe(Fw *myfw) {
    //usleep(5000);
    //writeFPGA(myfw,"/BCP_TCDS/LEMO_Config", 0x00030001);
    // stobe fast command sequence for all towers
    //usleep(5000);
    writeFPGA(myfw,"/BCP_TCDS/Fast_cmd_strobe", 0);
    //usleep(5000); 
    writeFPGA(myfw,"/BCP_TCDS/Fast_cmd_strobe", 1);
    //usleep(5000); // Sleep not necessary
    writeFPGA(myfw,"/BCP_TCDS/Fast_cmd_strobe", 0);
    //usleep(5000);
}

// This function is based on the idea that the fw version is in the first register of the fpga node
uint32_t getFirmware(std::string devnode) {

  void * memrange = NULL;
  if (easymem_map_uio(&memrange, devnode.c_str(), 0, 0x2000, 0) != 0) {
    std::cout<<"Unable to map UIO during fw version readout"<<std::endl;
    throw;
  }
  uint32_t data[1];
  easymem_saferead32(memrange, 0x0, 1, data, 1);
  return data[0];
}

void i2c_master_get_reg_offsets(int master_id, int &offset_wr, int &offset_rd) {
        assert(master_id<3); //"Invalid I2C master ID"

        offset_wr = master_id * ( LpgbtRegisterMap::I2CM1CMD -  LpgbtRegisterMap::I2CM0CMD);
        offset_rd = master_id * ( LpgbtRegisterMap::I2CM1STATUS -  LpgbtRegisterMap::I2CM0STATUS);
}

// The following function will be used for both mlpgbt->slpgbt and slpgb->vfe communication
// If mlpgbt->slpgbt:
// -    offset_wr = from i2c_master_get_reg_offsets with master_id = 2 (probably)
// -    lpgbt_device_add = mlpgbt_device_add
// -    i2c_slave_address = slpgbt_device_add
// If slpgb->vfe:
// -    offset_wr = from i2c_master_get_reg_offsets with master_id = 1,2 depending on the VFE num
// -    lpgbt_device_add = slpgbt_device_add
// -    i2c_slave_address = vfe_device_add
void i2c_master_set_slave_address(Fw *myfw, int tower, int offset_wr, int lpgbt_device_add, int i2c_slave_address) {

        int address_low = i2c_slave_address & 0x7F;
        int address_high = (i2c_slave_address >> 7) & 0x07;
        //WriteLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0ADDRESS.address + offset_wr, address_low, tower=tower)
        writeLpgbt(myfw, tower, lpgbt_device_add, LpgbtRegisterMap::I2CM0ADDRESS + offset_wr, address_low);

}

// The following function will be used for both mlpgbt->slpgbt and slpgb->vfe communication
// If mlpgbt->slpgbt:
// -    offset_wr = from i2c_master_get_reg_offsets with master_id = 2 (probably)
// -    lpgbt_device_add = mlpgbt_device_add
// If slpgb->vfe:
// -    offset_wr = from i2c_master_get_reg_offsets with master_id = 1,2 depending on the VFE num
// -    lpgbt_device_add = slpgbt_device_add
void i2c_master_issue_command(Fw *myfw, int tower, int offset_wr, int lpgbt_device_add, int command) {

        writeLpgbt(myfw, tower, lpgbt_device_add, LpgbtRegisterMap::I2CM0CMD + offset_wr, command);
}

// The following function will be used for both mlpgbt->slpgbt and slpgb->vfe communication
// If mlpgbt->slpgbt:
// -    offset_wr, offset_rd = from i2c_master_get_reg_offsets with master_id = 2 (probably)
// -    lpgbt_device_add = mlpgbt_device_add
// If slpgb->vfe:
// -    offset_wr, offset_rd = from i2c_master_get_reg_offsets with master_id = 1,2 depending on the VFE num
// -    lpgbt_device_add = slpgbt_device_add
void i2c_master_set_nbytes(Fw *myfw, int tower, int lpgbt_device_add, int offset_wr, int offset_rd, int nbytes, int freq=3) {
        assert(nbytes<16); //"Invalid transaction length"

        int control_reg_val = readLpgbt(myfw, tower, lpgbt_device_add, LpgbtRegisterMap::I2CM0CTRL + offset_rd);
        control_reg_val &= (~0x7C);         //~LpgbtEnumsV1.I2cmConfigReg.NBYTES.bit_mask
        control_reg_val &= (~3);            //~LpgbtEnumsV1.I2cmConfigReg.FREQ.bit_mask

        control_reg_val |= nbytes << 2;     //LpgbtEnumsV1.I2cmConfigReg.NBYTES.offset
        control_reg_val |= freq << 0;       //LpgbtEnumsV1.I2cmConfigReg.FREQ.offset
        writeLpgbt(myfw, tower, lpgbt_device_add, LpgbtRegisterMap::I2CM0DATA0 + offset_wr, control_reg_val);
        i2c_master_issue_command(myfw, tower, offset_wr, lpgbt_device_add, I2cmCommand::WRITE_CRA);
}

// The following function will be used for both mlpgbt->slpgbt and slpgb->vfe communication
// If mlpgbt->slpgbt:
// -    offset_wr = from i2c_master_get_reg_offsets with master_id = 2 (probably)
// -    lpgbt_device_add = mlpgbt_device_add
// If slpgb->vfe:
// -    offset_wr = from i2c_master_get_reg_offsets with master_id = 1,2 depending on the VFE num
// -    lpgbt_device_add = slpgbt_device_add
void i2c_master_await_completion(Fw *myfw, int tower, int lpgbt_device_add, int offset_rd, int timeout=100) {
    auto start = std::chrono::high_resolution_clock::now();
    while (true) {

        int status = readLpgbt(myfw, tower, lpgbt_device_add, LpgbtRegisterMap::I2CM0STATUS + offset_rd);

        if (status & 0x04) break;//LpgbtEnumsV1.I2cmStatusReg.SUCC.bit_mask:
        if (status & 0x08) { //LpgbtEnumsV1.I2cmStatusReg.LEVEERR.bit_mask):
            LOGGER->log_message(LogManager::DEBUG, stdsprintf("The SDA line is pulled low before initiating a transaction from %d", lpgbt_device_add));
            throw std::runtime_error("SDA line is pulled low");
        }
        if (status & 0x40) { //LpgbtEnumsV1.I2cmStatusReg.NOACK.bit_mask:
            LOGGER->log_message(LogManager::DEBUG, stdsprintf("The last transaction was not acknowledged by the I2C slave from %d", lpgbt_device_add));
            //throw;
            throw std::runtime_error("Transaction was not acknowledged");
        }
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        if (duration.count() > timeout) {
            LOGGER->log_message(LogManager::DEBUG, stdsprintf("Timeout while waiting for I2C master to finish from %d", lpgbt_device_add));
            throw;
        }
        
    }
}

int readSlaveLpgbtI2C(
    Fw *myfw, 
    int tower, 
    int slpgbt_device_add, 
    int slpgbt_reg_add, 
    int nb_bytes=1, 
    int mlpgbt_i2c_ch=2, 
    int mlpgbt_device_add=113, 
    int slpgbt_reg_width=2, 
    int clk_freq=0, 
    int timeout=100) {

    nb_bytes=1; //TO CHANGE


    int offset_wr, offset_rd;
    i2c_master_get_reg_offsets(mlpgbt_i2c_ch, offset_wr, offset_rd);
    i2c_master_set_slave_address(myfw, tower, offset_wr, mlpgbt_device_add, slpgbt_device_add);
    for (int i=0; i<slpgbt_reg_width; i++) 
        writeLpgbt(myfw, tower, mlpgbt_device_add, LpgbtRegisterMap::I2CM0DATA0 + offset_wr + i, (slpgbt_reg_add >> (8 * i)) & 0xFF);
    
    i2c_master_issue_command(myfw, tower, offset_wr, mlpgbt_device_add, I2cmCommand::W_MULTI_4BYTE0);
    i2c_master_set_nbytes(myfw, tower, mlpgbt_device_add, offset_wr, offset_rd, slpgbt_reg_width, clk_freq);
    i2c_master_issue_command(myfw, tower, offset_wr, mlpgbt_device_add, I2cmCommand::WRITE_MULTI);
    i2c_master_await_completion(myfw, tower, mlpgbt_device_add, offset_rd, timeout=timeout);

    i2c_master_set_nbytes(myfw, tower, mlpgbt_device_add, offset_wr, offset_rd, nb_bytes, clk_freq);
    i2c_master_issue_command(myfw, tower, offset_wr, mlpgbt_device_add, I2cmCommand::READ_MULTI);
    i2c_master_await_completion(myfw, tower, mlpgbt_device_add, offset_rd, timeout);
    
    int result_address = LpgbtRegisterMap::I2CM0READ15 + offset_rd;
    return readLpgbt(myfw, tower, mlpgbt_device_add, result_address);
}

void writeSlaveLpgbtI2C(
    Fw *myfw,
    int tower,
    int slpgbt_device_add,
    int slpgbt_reg_add,
    int data,
    int mlpgbt_i2c_ch=2,
    int mlpgbt_device_add=113,
    int slpgbt_reg_width=2,
    int clk_freq=0,
    int timeout=100) {

    int offset_wr, offset_rd;
    i2c_master_get_reg_offsets(mlpgbt_i2c_ch, offset_wr, offset_rd);

    std::vector<int> address_and_data;
    for (int i=0; i<slpgbt_reg_width; i++) address_and_data.push_back((slpgbt_reg_add >> (8 * i)) & 0xFF);

    address_and_data.push_back(data);
    assert(address_and_data.size() <16); //"Unsupported I2C write length"

    i2c_master_set_nbytes(myfw, tower, mlpgbt_device_add, offset_wr, offset_rd, address_and_data.size(), clk_freq);
    
    for(int i=0; i<address_and_data.size(); i++) {
      writeLpgbt(myfw, tower, mlpgbt_device_add, LpgbtRegisterMap::I2CM0DATA0 + offset_wr + (i % 4), address_and_data.at(i));
      if ((i % 4 == 3) || (i == (address_and_data.size() - 1))) {
          i2c_master_issue_command(myfw, tower, offset_wr, mlpgbt_device_add, I2cmCommand::W_MULTI_4BYTE0 + (i / 4));
        }
    }

    i2c_master_set_slave_address(myfw, tower, offset_wr, mlpgbt_device_add, slpgbt_device_add);
    i2c_master_issue_command(myfw, tower, offset_wr, mlpgbt_device_add, I2cmCommand::WRITE_MULTI);
    i2c_master_await_completion(myfw, tower, mlpgbt_device_add, offset_rd, timeout);


}

void writeLpgbtEC(Fw *myfw, int tower, int lpgbt_add, int reg_add, int data) {
  int tower_in_group = tower%3;
  int tower_group = int(tower)/3;
  
  std::ostringstream reg_string;
  reg_string << stdsprintf("/BCP_Lpgbt/tg%d/slow_cmd_group/", tower_group);

  uint32_t new_val = 0;
  // Falcon firmware requires switching between ec and ic
  // Falcon firmware doesn't require mux
  if (FALCON) {
    if (lpgbt_add == 113) writeFPGA(myfw, "/BCP_Lpgbt/ec_ic", 0x0);
    else writeFPGA(myfw, "/BCP_Lpgbt/ec_ic", 0x8);
  } else {
    new_val = (tower_in_group << 1) + (lpgbt_add != 113);
    writeFPGA(myfw, reg_string.str()+"reg_ctrl_mux", new_val);
  }
  writeFPGA(myfw, reg_string.str()+"ic_empty", data);
  writeFPGA(myfw, reg_string.str()+"mem_addr", reg_add);
  new_val = (2 << 8) + lpgbt_add;
  writeFPGA(myfw, reg_string.str()+"lpGBT_addr", new_val);
}

int readLpgbtEC(Fw *myfw, int tower, int lpgbt_add, int reg_add) {
  int tower_in_group = tower%3;
  int tower_group = int(tower)/3;
  int nb_bytes = 1;
  std::ostringstream reg_string;
  reg_string << stdsprintf("/BCP_Lpgbt/tg%d/slow_cmd_group/", tower_group);
  
  uint32_t new_val = 0;
  // Falcon firmware requires switching between ec and ic
  // Falcon firmware doesn't require mux
  if (FALCON) {
    if (lpgbt_add == 113) writeFPGA(myfw, "/BCP_Lpgbt/ec_ic", 0x0);
    else writeFPGA(myfw, "/BCP_Lpgbt/ec_ic", 0x8);
  } else { 
    new_val = (tower_in_group << 1) + (lpgbt_add != 113);
    writeFPGA(myfw, reg_string.str()+"reg_ctrl_mux", new_val);
  }

  new_val = (nb_bytes << 16) + reg_add;
  writeFPGA(myfw, reg_string.str()+"mem_addr", new_val);

  new_val = (1 << 8) + lpgbt_add;
  writeFPGA(myfw, reg_string.str()+"lpGBT_addr", new_val);

  bool full = true;
  auto start = std::chrono::high_resolution_clock::now();
  while (full) {
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    if (duration.count() > 100000) {
      std::cout<< "readLpgbtEC timeout 1" << std::endl;
      throw;
    }
    int real_val = readFPGA(myfw, reg_string.str()+"ic_empty");
    full = (real_val & 0x1);
  }

  bool empty = false;
  start = std::chrono::high_resolution_clock::now();
  std::vector<uint32_t> bs;
  while (!empty) {
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    if (duration.count() > 100000) {
      std::cout<< "readLpgbtEC timeout 2" << std::endl;
      throw;
    }
    int real_val = readFPGA(myfw, reg_string.str()+"ic_empty");
    empty = (real_val & 0x1);
    uint32_t memval = (readFPGA(myfw, reg_string.str()+"mem_val") & 0xFF);
    bs.push_back(memval);
  }

  if (bs.size() >= 7) return bs[6];
  else {
    std::cout<< "readLpgbtEC response not long enough" << std::endl;
    throw;
  }
  return 0;
}

void writeLpgbt(Fw *myfw, int tower, int lpgbt_add, int reg_add, int data, bool ec_comm) {
  if (lpgbt_add != 113 and !ec_comm) {
    writeSlaveLpgbtI2C(myfw, tower=tower, lpgbt_add=lpgbt_add, reg_add=reg_add, data=data);
  } else
    writeLpgbtEC(myfw, tower=tower, lpgbt_add=lpgbt_add, reg_add=reg_add, data=data);
}

// For the moment this function reads only one byte from the lpgbts
int readLpgbt(Fw *myfw, int tower, int lpgbt_add, int reg_add, bool ec_comm) {
  if (lpgbt_add != 113 and !ec_comm)
    return readSlaveLpgbtI2C(myfw, tower=tower, lpgbt_add=lpgbt_add, reg_add=reg_add);
  return readLpgbtEC(myfw, tower=tower, lpgbt_add=lpgbt_add, reg_add=reg_add);
}

void writeVTRx(Fw *myfw, int tower, int vtrx_device_add, int vtrx_reg_add, int data) {
    writeSlaveLpgbtI2C(myfw,
    tower,
    vtrx_device_add,
    vtrx_reg_add,
    data,
    1,
    113,
    1,
    0,
    100);
}

int readVTRx(Fw *myfw, int tower, int vtrx_device_add, int vtrx_reg_add) {
    return readSlaveLpgbtI2C(
        myfw,
        tower,
        vtrx_device_add,
        vtrx_reg_add,
        1,
        1,
        113,
        1,
        0,
        100);
}

void fromVFEnumToMap(int vfenumber, int &slpgbt_add, int &slpgbt_i2c_num) {

    slpgbt_add = 114;
    slpgbt_i2c_num = 1;
    if (vfenumber == 0) {
        slpgbt_add = 114;
        slpgbt_i2c_num = 1;
    } else if (vfenumber == 1) {
        slpgbt_add = 114;
        slpgbt_i2c_num = 2;
    } else if (vfenumber == 2) {
        slpgbt_add = 115;
        slpgbt_i2c_num = 1;
    } else if (vfenumber == 3) {
        slpgbt_add = 115;
        slpgbt_i2c_num = 2;
    } else if (vfenumber == 4) {
      slpgbt_add = 116;
      slpgbt_i2c_num = 1;
    }

}

// TODO: can we join this function with writeSlaveLpgbtI2C?????
void writeVFEDevice(Fw *myfw, int tower, int VFE_num, int VFE_device_add, int VFE_reg_add, int VFE_reg_width, int data, int timeout=100) {

    int slpgbt_device_add, slpgbt_i2c_ch;
    fromVFEnumToMap(VFE_num, slpgbt_device_add, slpgbt_i2c_ch);

    int offset_wr_slpgbt, offset_rd_slpgbt;
    i2c_master_get_reg_offsets(slpgbt_i2c_ch, offset_wr_slpgbt, offset_rd_slpgbt);

    std::vector<int> address_and_data;
    for (int i=0; i<VFE_reg_width; i++) address_and_data.push_back((VFE_reg_add >> (8 * i)) & 0xFF);

    address_and_data.push_back(data);
    assert(address_and_data.size() <16); //"Unsupported I2C write length"

    i2c_master_set_nbytes(myfw, tower, slpgbt_device_add, offset_wr_slpgbt, offset_rd_slpgbt, address_and_data.size());

    for(int i=0; i<address_and_data.size(); i++) {
      writeLpgbt(myfw, tower, slpgbt_device_add, LpgbtRegisterMap::I2CM0DATA0 + offset_wr_slpgbt + (i % 4), address_and_data.at(i));
      if ((i % 4 == 3) || (i == (address_and_data.size() - 1))) {
          i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::W_MULTI_4BYTE0 + (i / 4));
        }
    }

    i2c_master_set_slave_address(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, VFE_device_add);
    i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::WRITE_MULTI);
    i2c_master_await_completion(myfw, tower, slpgbt_device_add, offset_rd_slpgbt, timeout);

}

void multiWriteVFEDevice(Fw *myfw, int tower, int VFE_num, int VFE_device_add, int VFE_reg_add, int VFE_reg_width, std::vector<int> data, int timeout=100) {

    int slpgbt_device_add, slpgbt_i2c_ch;
    fromVFEnumToMap(VFE_num, slpgbt_device_add, slpgbt_i2c_ch);

    int offset_wr_slpgbt, offset_rd_slpgbt;
    i2c_master_get_reg_offsets(slpgbt_i2c_ch, offset_wr_slpgbt, offset_rd_slpgbt);
    
    std::vector<int> address_and_data;
    for (int i=0; i<VFE_reg_width; i++) address_and_data.push_back((VFE_reg_add >> (8 * i)) & 0xFF);
    
    for(const int& single_byte : data) 
      address_and_data.push_back(single_byte);
    assert(address_and_data.size() <16); //"Unsupported I2C write length"

    i2c_master_set_nbytes(myfw, tower, slpgbt_device_add, offset_wr_slpgbt, offset_rd_slpgbt, address_and_data.size());

    for(int i=0; i<address_and_data.size(); i++) {
      writeLpgbt(myfw, tower, slpgbt_device_add, LpgbtRegisterMap::I2CM0DATA0 + offset_wr_slpgbt + (i % 4), address_and_data.at(i));
      if ((i % 4 == 3) || (i == (address_and_data.size() - 1))) {
          i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::W_MULTI_4BYTE0 + (i / 4));
        } 
    }

    i2c_master_set_slave_address(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, VFE_device_add);
    i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::WRITE_MULTI);
    i2c_master_await_completion(myfw, tower, slpgbt_device_add, offset_rd_slpgbt, timeout);
    
}

// This function is used to read only one byte. We hardcode nb_bytes=1.
int readVFEDevice(
    Fw *myfw,
    int tower,
    int VFE_num,
    int VFE_device_add,
    int VFE_reg_add,
    int VFE_reg_width,
    int freq=0,
    int timeout=100
) {

    int nb_bytes=1;

    int slpgbt_device_add, slpgbt_i2c_ch;
    fromVFEnumToMap(VFE_num, slpgbt_device_add, slpgbt_i2c_ch);

    int offset_wr_slpgbt, offset_rd_slpgbt;
    i2c_master_get_reg_offsets(slpgbt_i2c_ch, offset_wr_slpgbt, offset_rd_slpgbt);
 
    i2c_master_set_slave_address(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, VFE_device_add);
    for (int i=0; i<VFE_reg_width; i++)
        writeLpgbt(myfw, tower, slpgbt_device_add, LpgbtRegisterMap::I2CM0DATA0 + offset_wr_slpgbt + i, (VFE_reg_add >> (8 * i)) & 0xFF);


    i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::W_MULTI_4BYTE0);
    i2c_master_set_nbytes(myfw, tower, slpgbt_device_add, offset_wr_slpgbt, offset_rd_slpgbt, VFE_reg_width);
    i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::WRITE_MULTI);
    i2c_master_await_completion(myfw, tower, slpgbt_device_add, offset_rd_slpgbt, timeout);
    i2c_master_set_nbytes(myfw, tower, slpgbt_device_add, offset_wr_slpgbt, offset_rd_slpgbt, nb_bytes);
    i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::READ_MULTI);
    i2c_master_await_completion(myfw, tower, slpgbt_device_add, offset_rd_slpgbt, timeout);
         
    int result_address = LpgbtRegisterMap::I2CM0READ15 + offset_rd_slpgbt;
    return readLpgbt(myfw, tower, slpgbt_device_add, result_address);
}

//TODO: remove nb_bytes and make size of list
void multiReadVFEDevice(
    Fw *myfw,
    int tower,
    int VFE_num,
    int VFE_device_add,
    int VFE_reg_add,
    int VFE_reg_width,
    std::vector<int>& bulk_data,
    int nb_bytes=1,
    int freq=0,
    int timeout=100
) {

    int slpgbt_device_add, slpgbt_i2c_ch;
    fromVFEnumToMap(VFE_num, slpgbt_device_add, slpgbt_i2c_ch);

    int offset_wr_slpgbt, offset_rd_slpgbt;
    i2c_master_get_reg_offsets(slpgbt_i2c_ch, offset_wr_slpgbt, offset_rd_slpgbt);

    i2c_master_set_slave_address(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, VFE_device_add);
    for (int i=0; i<VFE_reg_width; i++)
        writeLpgbt(myfw, tower, slpgbt_device_add, LpgbtRegisterMap::I2CM0DATA0 + offset_wr_slpgbt + i, (VFE_reg_add >> (8 * i)) & 0xFF);


    i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::W_MULTI_4BYTE0);
    i2c_master_set_nbytes(myfw, tower, slpgbt_device_add, offset_wr_slpgbt, offset_rd_slpgbt, VFE_reg_width);
    i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::WRITE_MULTI);
    i2c_master_await_completion(myfw, tower, slpgbt_device_add, offset_rd_slpgbt, timeout);
    i2c_master_set_nbytes(myfw, tower, slpgbt_device_add, offset_wr_slpgbt, offset_rd_slpgbt, nb_bytes);
    i2c_master_issue_command(myfw, tower, offset_wr_slpgbt, slpgbt_device_add, I2cmCommand::READ_MULTI);
    i2c_master_await_completion(myfw, tower, slpgbt_device_add, offset_rd_slpgbt, timeout);

    for (int i=0; i<nb_bytes; i++) {
        
        int result_address = LpgbtRegisterMap::I2CM0READ15 + offset_rd_slpgbt - i;
        int read_val = readLpgbt(myfw, tower, slpgbt_device_add, result_address);
	bulk_data.push_back(read_val);
    }
}
#endif
