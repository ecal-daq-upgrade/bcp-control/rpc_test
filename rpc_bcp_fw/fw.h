#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>

#ifndef FW_CLASS
#define FW_CLASS
struct Reg {
  unsigned int offset;
  unsigned int bitsize;
  unsigned int bitoffset;
};

class Fw {
  public:
    Fw(LogManager* outlogger, std::string register_map_filepath) {
      LOGGER = outlogger;
      map_is_open = false;
      LOGGER->log_message(LogManager::INFO, "In Fw constructor");
      populate_map_from_csv(register_map_filepath);
    };
    
    std::map<std::string,Reg> reg_map;
    void* memrange = NULL;
    bool map_is_open;
    LogManager * LOGGER;

    void populate_map_from_csv(std::string register_map_filepath) {
      
      auto start = std::chrono::high_resolution_clock::now();
      LOGGER->log_message(LogManager::INFO, "In populate_map_from_csv ");
      LOGGER->log_message(LogManager::INFO, "[Fw::populate_map_from_csv] Register map file: " + register_map_filepath);

      std::ifstream infile(register_map_filepath);
      std::string line,str;
      std::string parent = "";
      std::string name = "";
      unsigned int offset = 0;
      unsigned int bitsize = 0;
      unsigned int bitoffset = 0;
      bool firstline = true;
      if (infile.is_open()) {
        while (getline(infile, line)) {
                if (firstline) {
                  firstline = false;
                  continue;
                }
                std::stringstream ss(line);

                // Use while loop to check the getline() function condition.
                std::string parent = "";
                int counter = 0;
                try {
                  while (getline(ss, str, ',')) {
                          if (counter == 0) parent = str;
                          if (counter == 1) name = str;
                          if (counter == 2) {
				  if (FALCON) offset = stoul(str.substr(2),0,16);
			  	  else offset = stoul(str.substr(2),0,16)-0x60000000;
			  }
                          if (counter == 3) bitsize = stoul(str);
                          if (counter == 4) bitoffset = stoul(str);
                        counter ++;
                  }
                } catch (std::invalid_argument iaex) {
                  std::cout <<"-"<< str <<"-\n";
                  break;
                }
                std::replace( parent.begin(), parent.end(), '*', '/');
                reg_map.insert({ parent+"/"+name, Reg {offset, bitsize, bitoffset} });
        }
        infile.close();
      } else {
        LOGGER->log_message(LogManager::ERROR, "[Fw::populate_map_from_csv] Register map file not open, check if it exists");
      }
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
      LOGGER->log_message(LogManager::INFO, stdsprintf("Register map populated in us%d", duration.count()));
    }

    uint32_t getMemSize() {
      std::map<std::string,Reg>::iterator regmapit;
      uint32_t max_offset = 0;
      for (regmapit = reg_map.begin(); regmapit != reg_map.end(); regmapit++) {
        if (regmapit->second.offset > max_offset) { 
		    max_offset=regmapit->second.offset;
    	}
      }
      uint32_t mem_size = ((max_offset + 4)/ 4096) + 1;
      return mem_size*4096;
    }

    void openMap() {
      uint32_t memsize = getMemSize();
      LOGGER->log_message(LogManager::INFO, stdsprintf("MemSize extracted from fw map %d", memsize));

      std::string map_string = "";
      if (FALCON) map_string = "/dev/uio.TranslateAxiAddr_0";
      else map_string = "/dev/uio.axi_chip2chip_0";

      if (easymem_map_uio(&memrange, map_string.c_str(), 0, memsize, 0) != 0) {
          LOGGER->log_message(LogManager::ERROR, "Unable to map UIO");
	      throw;
      }
      map_is_open = true;
      LOGGER->log_message(LogManager::INFO, "map is open");
    }

    void * getMemPointer() {
      if (memrange == NULL) openMap();
      return memrange;
    }

    
};
#endif
