# rpc_test
Block of configuration files to run with apx-prime
For instance, command:
```shell
apx-prime -H elm10011 -c config_not_DTH.ini
```
In the repository there are no .bit files. You need to move the firmware files in the rpc_test folder or change the path in the config file.

