// Compile and launch

// g++ -std=gnu++11  -o mytest mytest.cpp -I/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -L/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev -lwiscrpcsvc
// export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/hdda/nfs/elm_shared/apx-prime/rpcclient/rpcsvc_client_dev
// ./mytest


#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wiscrpcsvc.h>
#include "../rpc_bcp_fw/json.hpp"
using json = nlohmann::json;
using namespace wisc;


#define MEMORY_TEST

json fromCfgToJson(std::string config_file) {
        
        std::cout << "Parse configuration from file " + config_file <<std::endl;
        std::ifstream infile(config_file);
        std::string line;
        json j;
        
        int add, val;
        while (std::getline(infile, line)) {
            if (line.rfind("#", 0) == 0) continue; // Found a comment
            std::istringstream iss(line);
            if (!(iss >> std::hex >> add >> val)) break;
            j[std::to_string(add)] = val;
        }
        std::cout<<"questo"<<j<<std::endl;
        return j;
}


int main(int argc, char *argv[]) {

    RPCSvc rpc;
    try {
        rpc.connect(argv[1]);
    }
    catch (RPCSvc::ConnectionFailedException &e) {
        printf("Caught RPCErrorException: %s\n", e.message.c_str());
        return 1;
    }
    catch (RPCSvc::RPCException &e) {
        printf("Caught exception: %s\n", e.message.c_str());
        return 1;
    }

#define STANDARD_CATCH                                                   \
    catch (RPCSvc::NotConnectedException & e) {                          \
        printf("Caught NotConnectedException: %s\n", e.message.c_str()); \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCErrorException & e) {                              \
        printf("Caught RPCErrorException: %s\n", e.message.c_str());     \
        return 1;                                                        \
    }                                                                    \
    catch (RPCSvc::RPCException & e) {                                   \
        printf("Caught exception: %s\n", e.message.c_str());             \
        return 1;                                                        \
    }

#define ASSERT(x)                                                      \
    do {                                                               \
        if (!(x)) {                                                    \
            printf("Assertion Failed on line %u: %s\n", __LINE__, #x); \
            return 1;                                                  \
        }                                                              \
    } while (0)

    RPCMsg req, rsp;

#ifdef MEMORY_TEST
    try {
        ASSERT(rpc.load_module("fwmodule", "fwmodule v1.0.2"));
    }
    STANDARD_CATCH;
    
    req = RPCMsg("fwmodule.configurefe");
    rsp = RPCMsg();
    try {
        std::cout<<"Calling fwmodule.configurefe" <<std::endl;
        json mlpgbt_json = fromCfgToJson("../config_files/lpgbts/master_rom_ec_v33.cnf");
        json slpgbt1_json = fromCfgToJson("../config_files/lpgbts/slave_1_v33.cnf");
        json slpgbt2_json = fromCfgToJson("../config_files/lpgbts/slave_v33.cnf");
        json slpgbt3_json = fromCfgToJson("../config_files/lpgbts/slave_v33.cnf");
        json all_lpgbts;
        all_lpgbts[std::to_string(113)] = mlpgbt_json;
        all_lpgbts[std::to_string(114)] = slpgbt1_json;
        all_lpgbts[std::to_string(115)] = slpgbt2_json;
        all_lpgbts[std::to_string(116)] = slpgbt3_json;
        std::cout<<"questo"<< all_lpgbts<<std::endl;
        req.set_string("lpgbtsjsonstring", all_lpgbts.dump()); 
        req.set_word("towernb", 0);
	rsp = rpc.call_method(req);
    }
    STANDARD_CATCH;


#endif


    return 0;
}
